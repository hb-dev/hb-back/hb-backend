# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20210304152557) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "authentication_tokens", force: :cascade do |t|
    t.string   "body"
    t.string   "ip_address"
    t.string   "user_agent"
    t.datetime "last_used_at"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["user_id"], name: "index_authentication_tokens_on_user_id", using: :btree
  end

  create_table "button_net_tags", force: :cascade do |t|
    t.integer  "button_net_id"
    t.integer  "tag_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["button_net_id"], name: "index_button_net_tags_on_button_net_id", using: :btree
    t.index ["tag_id"], name: "index_button_net_tags_on_tag_id", using: :btree
  end

  create_table "button_nets", force: :cascade do |t|
    t.string   "name",                           null: false
    t.text     "description",                    null: false
    t.integer  "creator_id",                     null: false
    t.string   "img_url",                        null: false
    t.float    "latitude",                       null: false
    t.float    "longitude",                      null: false
    t.string   "location_name",                  null: false
    t.boolean  "privacy",                        null: false
    t.string   "net_url"
    t.integer  "net_options",    default: [],                 array: true
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "active",         default: false
    t.integer  "blocked_users",  default: [],                 array: true
    t.integer  "friend_nets_id", default: [],                 array: true
    t.integer  "allowed_users",  default: [],                 array: true
    t.integer  "other_net_ids",  default: [],                 array: true
    t.integer  "admin_users",    default: [],                 array: true
    t.integer  "tags_id"
    t.boolean  "nickname",       default: false
    t.index ["tags_id"], name: "index_button_nets_on_tags_id", using: :btree
  end

  create_table "button_tags", force: :cascade do |t|
    t.integer  "tag_type"
    t.integer  "button_id"
    t.integer  "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["button_id"], name: "index_button_tags_on_button_id", using: :btree
    t.index ["tag_id"], name: "index_button_tags_on_tag_id", using: :btree
  end

  create_table "buttons", force: :cascade do |t|
    t.text     "description"
    t.string   "full_address"
    t.string   "date"
    t.string   "location_name"
    t.float    "latitude"
    t.float    "longitude"
    t.float    "to_latitude"
    t.float    "to_longitude"
    t.integer  "button_type",          default: 0
    t.boolean  "active",               default: false
    t.boolean  "deactivated_user",     default: false
    t.boolean  "swap"
    t.integer  "creator_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "shared_counter",       default: 0
    t.string   "to_location_name"
    t.boolean  "mail_schedule_sended", default: false
    t.string   "periodic_date"
    t.integer  "button_net_id",        default: [],                 array: true
    t.boolean  "multi",                default: false
    t.index ["button_type"], name: "index_buttons_on_button_type", using: :btree
    t.index ["creator_id"], name: "index_buttons_on_creator_id", using: :btree
  end

  create_table "chats", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "button_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "multi",      default: false
    t.index ["button_id"], name: "index_chats_on_button_id", using: :btree
    t.index ["user_id"], name: "index_chats_on_user_id", using: :btree
  end

  create_table "images", force: :cascade do |t|
    t.boolean  "provisional",       default: false
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.string   "image_property"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.integer  "icon_id"
    t.index ["icon_id"], name: "index_images_on_icon_id", using: :btree
    t.index ["imageable_id"], name: "index_images_on_imageable_id", using: :btree
  end

  create_table "languages", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "languages_users", id: false, force: :cascade do |t|
    t.integer "language_id", null: false
    t.integer "user_id",     null: false
    t.index ["language_id"], name: "index_languages_users_on_language_id", using: :btree
    t.index ["user_id"], name: "index_languages_users_on_user_id", using: :btree
  end

  create_table "messages", force: :cascade do |t|
    t.text     "body"
    t.boolean  "read",       default: false
    t.integer  "chat_id"
    t.integer  "user_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["chat_id"], name: "index_messages_on_chat_id", using: :btree
    t.index ["user_id"], name: "index_messages_on_user_id", using: :btree
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.integer  "buttons_counter"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "active_buttons_counter"
    t.string   "search_name"
  end

  create_table "transfered_buttons", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "button_id",  null: false
    t.integer  "user_id",    null: false
    t.string   "email",      null: false
  end

  create_table "user_tags", force: :cascade do |t|
    t.integer  "tag_type"
    t.integer  "user_id"
    t.integer  "tag_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "button_nets", default: [],              array: true
    t.index ["tag_id"], name: "index_user_tags_on_tag_id", using: :btree
    t.index ["user_id"], name: "index_user_tags_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "nickname"
    t.string   "name"
    t.string   "description"
    t.string   "phone"
    t.string   "location"
    t.string   "app_language"
    t.string   "provider_id"
    t.string   "provider_name"
    t.boolean  "active",                 default: true
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "avatar_id"
    t.boolean  "show_phone",             default: false
    t.string   "user_telegram"
    t.boolean  "use_external_conv",      default: false
    t.boolean  "use_whatsapp",           default: false
    t.boolean  "use_telegram",           default: false
    t.boolean  "admin",                  default: false
    t.boolean  "deliver_interests",      default: false
    t.boolean  "super_admin",            default: false
    t.integer  "tags_distance",          default: 20
    t.float    "tags_latitude"
    t.float    "tags_longitude"
    t.integer  "blocked",                default: [],                 array: true
    t.integer  "blocked_by",             default: [],                 array: true
    t.integer  "likes",                  default: 0
    t.integer  "liked",                  default: [],                 array: true
    t.index ["avatar_id"], name: "index_users_on_avatar_id", using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "authentication_tokens", "users"
  add_foreign_key "buttons", "users", column: "creator_id"
  add_foreign_key "chats", "buttons"
  add_foreign_key "chats", "users"
  add_foreign_key "messages", "chats"
  add_foreign_key "messages", "users"
end
