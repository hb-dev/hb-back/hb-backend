class AddMailSheduleSendedToButtons < ActiveRecord::Migration[5.0]
  def change
    add_column :buttons, :mail_schedule_sended, :boolean, default: false
  end
end
