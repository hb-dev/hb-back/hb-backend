class AddActiveButtonsCounterToTags < ActiveRecord::Migration[5.0]
  def change
    add_column :tags, :active_buttons_counter, :integer
  end
end
