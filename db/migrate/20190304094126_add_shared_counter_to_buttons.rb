class AddSharedCounterToButtons < ActiveRecord::Migration[5.0]
  def change
    add_column :buttons, :shared_counter, :integer, default: 0
  end
end
