class CreateButtonNets < ActiveRecord::Migration[5.0]
  def change
    create_table :button_nets do |t|
      t.string :name, null: false
      t.text :description, null: false
      t.integer :net_tags, array: true, default: []
      t.integer :creator_id, null: false
      t.string :img_url, null: false
      t.float :latitude, null: false
      t.float :longitude, null: false
      t.string :location_name, null: false
      t.boolean :privacy, null: false
      t.string :net_url, null: false
      t.integer :blocked_users, array: true, default: []
      t.integer :net_options, array: true, default: []

      t.timestamps
    end
  end
end
