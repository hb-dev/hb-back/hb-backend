class AddNetsToUserTags < ActiveRecord::Migration[5.0]
  def change
    add_column :user_tags, :button_nets, :integer, array:true, default: []
  end
end
