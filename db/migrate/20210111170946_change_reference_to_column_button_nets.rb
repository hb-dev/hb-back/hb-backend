class ChangeReferenceToColumnButtonNets < ActiveRecord::Migration[5.0]
  def change
    remove_reference :buttons, :button_net, references: :button_nets
    add_column :buttons, :button_net_id, :integer
  end
end
