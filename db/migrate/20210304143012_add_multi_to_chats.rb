class AddMultiToChats < ActiveRecord::Migration[5.0]
  def change
    add_column :chats, :multi, :boolean
  end
end
