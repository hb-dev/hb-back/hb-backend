class CreateUserTags < ActiveRecord::Migration[5.0]
  def change
    create_table :user_tags do |t|
      t.integer :tag_type
      t.references :user
      t.references :tag
      t.timestamps
    end
  end
end
