class ChangeMulti < ActiveRecord::Migration[5.0]
  def up
    change_column :chats, :multi, :boolean, default: false
  end

  def down
    change_column :chats, :multi, :boolean, default: nil
  end
end
