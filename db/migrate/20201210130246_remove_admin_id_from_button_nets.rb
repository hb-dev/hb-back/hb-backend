class RemoveAdminIdFromButtonNets < ActiveRecord::Migration[5.0]
  def change
    remove_column :button_nets, :admin_users
  end
end
