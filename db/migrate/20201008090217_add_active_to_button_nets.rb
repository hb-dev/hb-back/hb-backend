class AddActiveToButtonNets < ActiveRecord::Migration[5.0]
  def change
    add_column :button_nets, :active, :boolean, default: false
  end
end
