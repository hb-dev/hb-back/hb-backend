class AddTagsDistanceToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :tags_distance, :integer, default: ""
  end
end
