class AddOtherNetIdsToButtonNets < ActiveRecord::Migration[5.0]
  def change
    add_column :button_nets, :other_net_ids, :integer, array:true, default: []
  end
end
