class AddButtonNetsIdToButton2 < ActiveRecord::Migration[5.0]
  def change
    # remove_reference :buttons, :button_nets, references: :categories
    rename_column :buttons, :button_nets_id, :button_category_id
  end
end
