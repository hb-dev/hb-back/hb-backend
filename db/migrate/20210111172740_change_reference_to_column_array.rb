class ChangeReferenceToColumnArray < ActiveRecord::Migration[5.0]
  def change
    remove_column :buttons, :button_net_id
    add_column :buttons, :button_net_id, :integer, array:true, default: []
  end
end
