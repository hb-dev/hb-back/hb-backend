class AddButtonNetsIdToButton3 < ActiveRecord::Migration[5.0]
  def change
    remove_reference :buttons, :button_category, references: :categories
    add_column :buttons, :button_nets_id, :integer
  end
end
