class ChangeTagDistanceForUserReference < ActiveRecord::Migration[5.0]
  def change
    remove_reference :buttons, :button_net_id, references: :button_nets
  end
end
