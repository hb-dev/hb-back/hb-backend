class ChangeTagDistanceForUser < ActiveRecord::Migration[5.0]
  def change
    change_column :users, :tags_distance, :integer, default: 20
  end
end
