class CreateLanguages < ActiveRecord::Migration[5.0]
  def change
    create_table :languages do |t|
      t.string :code
      t.string :name
      t.timestamps
    end

    create_join_table :languages, :users do |t|
      t.index :language_id
      t.index :user_id
    end
  end
end
