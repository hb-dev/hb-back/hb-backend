class AddNetArrayToButton < ActiveRecord::Migration[5.0]
  def change
    remove_column :buttons, :button_nets_id
    add_column :buttons, :button_nets_id, :integer, array:true, default: []
  end
end
