class CreateButtonTags < ActiveRecord::Migration[5.0]
  def change
    create_table :button_tags do |t|
      t.integer :tag_type
      t.references :button
      t.references :tag
      t.timestamps
    end
  end
end
