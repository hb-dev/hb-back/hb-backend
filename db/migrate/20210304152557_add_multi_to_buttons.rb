class AddMultiToButtons < ActiveRecord::Migration[5.0]
  def change
    add_column :buttons, :multi, :boolean, default: false
  end
end
