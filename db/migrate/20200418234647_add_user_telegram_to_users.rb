class AddUserTelegramToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :user_telegram, :string
  end
end
