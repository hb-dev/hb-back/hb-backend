class CreateImages < ActiveRecord::Migration[5.0]
  def change
    create_table :images do |t|
      t.boolean :provisional, default: false
      t.integer :imageable_id
      t.string :imageable_type
      t.string :image_property
      t.timestamps
    end
    add_attachment :images, :file
    add_index :images, :imageable_id
    add_reference :users, :avatar, references: :images
  end
end
