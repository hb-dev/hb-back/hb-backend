class AddToLocationNameToButton < ActiveRecord::Migration[5.0]
  def change
    add_column :buttons, :to_location_name, :string
  end
end
