class AddAdminUsersToButtonNets < ActiveRecord::Migration[5.0]
  def change
    add_column :button_nets, :admin_users, :integer, array:true, default: []
  end
end
