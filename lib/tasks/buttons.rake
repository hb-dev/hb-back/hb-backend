#     <Helpbuttons, collaborative network-making software>
#     Copyright (C) 2013-2121  Helpbuttons
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
namespace :buttons do
  desc "Notificar botones pasados de fecha"
  task notify_outdated: :environment do
    Button.all.each do |button|
      if !button.date.nil? && button.date != "Ahora" && button.mail_schedule_sended != true
        button.update(mail_schedule_sended: true)
        if button.date.to_date == Time.zone.today
          button.chats.each do |chat|
            UserNotifierMailer.outdated_button(chat.user, button).deliver
          end
        end
        if button.date.to_date < Time.zone.today
          button.update(active: false)
        end
        if button.date.to_date < Time.zone.today + 60 && button.active == false
          button.destroy
        end
      end
    end
  end
end
