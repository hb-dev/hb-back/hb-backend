# Helpbuttons.org / Botonesdeayuda.org

Helpbuttons is a free and open-source network creation software written in Ember and Ruby on Rails. It serves as the platform for Helpbuttons.org and other projects. This version is the first version opened to the public.

Helpbuttons project needs to become a big community to survive, if you like the idea of having a big open and free platform dedicated to collaboration please send a message to <help@helpbuttons.org>.

Also, if you want this project to survive please donate : https://helpbuttons.org/others?section=donate or susbscribe here : https://www.teaming.net/helpbuttons-org

* Interested in helping out?
** <help@helpbuttons.org>

Helpbuttons is the result of global collaboration and cooperation. The CREDITS
file lists technical contributors to the project. The COPYING file explains
Helpbuttons's copyright and license (GNU General Public License, version 3 or
later). Many thanks to the Helpbuttons community for testing and suggestions.

This is just the backend app, remember that you need to download the frontend too.

All documentation for Helpbuttons is available online
at http://www.helpbuttons.org/others

Helpbuttons.org - 2013-2021 (c) Angel Vazquez <angel@watchoutfreedom.org>
                         and the Helpbuttons.org Community
See COPYING and file headers for license info

Helpbuttons installation assistance
Angel Vazquez <angel@helpbuttons.org>/<angel@watchoutfreedom.org>
Carlos Silveira <carlos@helpbuttons.org>


# Starting the server

`bin/rails s -b 0.0.0.0`
