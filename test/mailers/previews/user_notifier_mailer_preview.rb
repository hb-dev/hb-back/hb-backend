# Preview all emails at http://localhost:3000/rails/mailers/user_notifier_mailer
class UserNotifierMailerPreview < ActionMailer::Preview

  def new_user
    UserNotifierMailer.new_user(User.first, "the_password")
  end

  def updated_user
    UserNotifierMailer.updated_user(User.first)
  end

  def new_button
    UserNotifierMailer.new_button(User.last, Button.last)
  end

  def updated_button
    UserNotifierMailer.updated_button(User.first, Button.last, nil)
  end

  def actived_button
    UserNotifierMailer.updated_button(User.first, Button.first, true)
  end

  def hided_button
    UserNotifierMailer.updated_button(User.first, Button.first, false)
  end

  def new_chat
    UserNotifierMailer.new_chat(User.second, Chat.first)
  end

  def new_message
    UserNotifierMailer.new_message(User.third, Message.first)
  end

  def transfered_button
    UserNotifierMailer.transfered_button(User.last, Button.last)
  end

  def transfered_noreg_button
    UserNotifierMailer.transfered_noreg_button(User.last, Button.last,User.last.email)
  end

  def outdated_button
    UserNotifierMailer.outdated_button(User.last, Button.last)
  end

  def button_tag
    UserNotifierMailer.button_tag(Tag.last,User.first, Button.last)
  end

end
