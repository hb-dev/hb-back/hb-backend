require 'test_helper'

class ButtonsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @button = buttons(:one)
  end

  test "should get index" do
    get buttons_url
    assert_response :success
  end

  test "should create button" do
    assert_difference('Button.count') do
      post buttons_url, params: { button: { creator_id: @button.creator_id, description: @button.description, image_id: @button.image_id, name: @button.name, state: @button.state, tags: @button.tags, type: @button.type, when: @button.when } }
    end

    assert_response 201
  end

  test "should show button" do
    get button_url(@button)
    assert_response :success
  end

  test "should update button" do
    patch button_url(@button), params: { button: { creator_id: @button.creator_id, description: @button.description, image_id: @button.image_id, name: @button.name, state: @button.state, tags: @button.tags, type: @button.type, when: @button.when } }
    assert_response 200
  end

  test "should destroy button" do
    assert_difference('Button.count', -1) do
      delete button_url(@button)
    end

    assert_response 204
  end
end
