ActiveModelSerializers.config.tap do |ams|
  ams.adapter = :json_api
  ams.jsonapi_resource_type = :singular
end
