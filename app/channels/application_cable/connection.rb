# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
module ApplicationCable
  class Connection < ActionCable::Connection::Base

    identified_by :current_user

    def connect
      self.current_user = find_user
    end

    protected
    def find_user
      user = User.find_by email: request.cookies['X-USER-EMAIL']
      token = Tiddle::TokenIssuer.build.find_token user, request.cookies['X-USER-TOKEN']
      reject_unauthorized_connection unless token
      user
    end
    
  end
end
