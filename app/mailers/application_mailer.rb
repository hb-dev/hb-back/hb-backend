class ApplicationMailer < ActionMailer::Base
  default from: 'help@helpbuttons.org'
  layout 'mailer'
end
