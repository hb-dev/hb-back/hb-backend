#     <Helpbuttons, collaborative network-making software>
#     Copyright (C) 2013-2121  Helpbuttons
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
class UserNotifierMailer < ApplicationMailer

  def new_user(user, password)
    @user = user
    @password = password
    mail( to: @user.email, subject: I18n.t('activerecord.mailers.user_notifier.new_user.subject'))
  end

  def updated_user(user)
    @user = user
    mail( to: @user.email, subject: I18n.t('activerecord.mailers.user_notifier.updated_user.subject'))
  end

  def new_button(user, button)
    @user = user
    if @user.active
      if user.avatar.nil?
        @avatar = nil
      else
        @avatar = user.avatar.asset_url_normal
      end
      @button = button
      if @button.image.nil?
        @buttonImage = nil
      else
        @buttonImage = @button.image.asset_url_normal
      end
      if @user.id != @button.creator.id
        if (!@user.tags_latitude.nil? && !@user.tags_longitude.nil?)
          @sw = Geokit::LatLng.new(@user.tags_latitude, @user.tags_longitude)
          if @user.tags_distance.nil?
            # Default distance
            bounds = Geokit::Bounds.from_point_and_radius(@sw, 1000 * 0.62137)
          else
            bounds = Geokit::Bounds.from_point_and_radius(@sw,@user.tags_distance * 0.62137)
          end
          @buttonArr = []
          @buttonArr.push(@button)
          @allButtons = Button.in_bounds(bounds);
          sendMail = @buttonArr.in_bounds(bounds)
        else
          sendMail = false
        end
      end
      sendMail = true

      if sendMail
        @redirectURL = "https://www.helpbuttons.org/button/" + @button.id.to_s
        if @button.date == "Ahora"
          @buttonDate = Time.current.strftime("%w, %d/%m/%Y")
          @buttonWeekDay = Time.current.strftime("%w")
          @buttonDay = Time.current.strftime("%d")
          @buttonMonth = Time.current.strftime("%m")
          @buttonTime = Time.now.strftime("%H:%M")
        else
          @buttonDate = Date.parse(@button.date.to_s).strftime("%w, %d/%m/%Y")
          @buttonWeekDay = Date.parse(@button.date.to_s).strftime("%w")
          @buttonDay = Date.parse(@button.date.to_s).strftime("%d")
          @buttonMonth = Date.parse(@button.date.to_s).strftime("%m")
          @buttonTime = Date.parse(@button.date.to_s).strftime("%H:%M")
        end
        case @buttonWeekDay
        when "1"
          @buttonWeekDay = "Lunes"
        when "2"
          @buttonWeekDay = "Martes"
        when "3"
          @buttonWeekDay = "Miércoles"
        when "4"
          @buttonWeekDay = "Jueves"
        when "5"
          @buttonWeekDay = "Viernes"
        when "6"
          @buttonWeekDay = "Sábado"
        when "0"
          @buttonWeekDay = "Domingo"
        else
          @buttonWeekDay = "Domingo"
        end

        case @buttonMonth
        when "1"
          @buttonMonth = "enero"
        when "2"
          @buttonMonth = "febrero"
        when "3"
          @buttonMonth = "marzo"
        when "4"
          @buttonMonth = "abril"
        when "5"
          @buttonMonth = "mayo"
        when "6"
          @buttonMonth = "junio"
        when "7"
          @buttonMonth = "julio"
        when "8"
          @buttonMonth = "agosto"
        when "9"
          @buttonMonth = "septiembre"
        when "10"
          @buttonMonth = "octubre"
        when "11"
          @buttonMonth = "noviembre"
        when "12"
          @buttonMonth = "diciembre"
        else
          @buttonMonth = "diciembre"
        end
        if @button.date == "Ahora"
          @buttonDate = @button.date
        else
          @buttonDate = @buttonWeekDay + ", " + @buttonDay + " de " + @buttonMonth + " a las " + @buttonTime
        end

        @isNeedType = false
        @isOfferType = false
        @isChangeType = false
        case @button.button_type
        when "need"
          @isNeedType = true
        when "offer"
          @isOfferType = true
        when "change"
          @isChangeType = true
        else
          @isChangeType = true
        end

        mail( to: @user.email, subject: I18n.t('activerecord.mailers.user_notifier.new_button.subject'))
      end
    end
  end

  def updated_button(user, button, check_button)
    @user = user
    @check_button = check_button

    if user.avatar.nil?
      @avatar = nil
    else
      @avatar = user.avatar.asset_url_normal
    end
    @button = button
    if @button.image.nil?
      @buttonImage = nil
    else
      @buttonImage = @button.image.asset_url_normal
    end
    @redirectURL = "https://www.helpbuttons.org/button/" + @button.id.to_s
    if @button.date == "Ahora"
      @buttonDate = Time.now.strftime("%w, %d/%m/%Y")
      @buttonWeekDay = Time.now.strftime("%w")
      @buttonDay = Time.now.strftime("%d")
      @buttonMonth = Time.now.strftime("%m")
      @buttonTime = Time.now.strftime("%H:%M")
    else
      @buttonDate = Date.parse(@button.date.to_s).strftime("%w, %d/%m/%Y")
      @buttonWeekDay = Date.parse(@button.date.to_s).strftime("%w")
      @buttonDay = Date.parse(@button.date.to_s).strftime("%d")
      @buttonMonth = Date.parse(@button.date.to_s).strftime("%m")
      @buttonTime = Date.parse(@button.date.to_s).strftime("%H:%M")
    end
    case @buttonWeekDay
    when "1"
      @buttonWeekDay = "Lunes"
    when "2"
      @buttonWeekDay = "Martes"
    when "3"
      @buttonWeekDay = "Miércoles"
    when "4"
      @buttonWeekDay = "Jueves"
    when "5"
      @buttonWeekDay = "Viernes"
    when "6"
      @buttonWeekDay = "Sábado"
    when "0"
      @buttonWeekDay = "Domingo"
    else
      @buttonWeekDay = "Domingo"
    end

    case @buttonMonth
    when "1"
      @buttonMonth = "enero"
    when "2"
      @buttonMonth = "febrero"
    when "3"
      @buttonMonth = "marzo"
    when "4"
      @buttonMonth = "abril"
    when "5"
      @buttonMonth = "mayo"
    when "6"
      @buttonMonth = "junio"
    when "7"
      @buttonMonth = "julio"
    when "8"
      @buttonMonth = "agosto"
    when "9"
      @buttonMonth = "septiembre"
    when "10"
      @buttonMonth = "octubre"
    when "11"
      @buttonMonth = "noviembre"
    when "12"
      @buttonMonth = "diciembre"
    else
      @buttonMonth = "diciembre"
    end

    if @button.date == "Ahora"
      @buttonDate = @button.date
    else
      @buttonDate = @buttonWeekDay + ", " + @buttonDay + " de " + @buttonMonth + " a las " + @buttonTime
    end

    @isNeedType = false
    @isOfferType = false
    @isChangeType = false
    case @button.button_type
    when "need"
      @isNeedType = true
    when "offer"
      @isOfferType = true
    when "change"
      @isChangeType = true
    else
      @isChangeType = true
    end

    if @check_button == true
      mail( to: @user.email, subject: I18n.t('activerecord.mailers.user_notifier.active_button.subject.active'))
    elsif @check_button == false
      mail( to: @user.email, subject: I18n.t('activerecord.mailers.user_notifier.active_button.subject.hide'))
    else
      mail( to: @user.email, subject: I18n.t('activerecord.mailers.user_notifier.updated_button.subject'))
    end
  end

  def button_tag(tagParam, user, button)
    @user = button.creator
    @interested = user
    if @user.avatar.nil?
      @avatar = nil
    else
      @avatar = @user.avatar.asset_url_normal
    end
    @button = button
    if @button.image.nil?
      @buttonImage = nil
    else
      @buttonImage = @button.image.asset_url_normal
    end
    @redirectURL = "https://www.helpbuttons.org/button/" + @button.id.to_s
    if @button.date == "Ahora"
      @buttonDate = Time.now.strftime("%w, %d/%m/%Y")
      @buttonWeekDay = Time.now.strftime("%w")
      @buttonDay = Time.now.strftime("%d")
      @buttonMonth = Time.now.strftime("%m")
      @buttonTime = Time.now.strftime("%H:%M")
    else
      @buttonDate = Date.parse(@button.date.to_s).strftime("%w, %d/%m/%Y")
      @buttonWeekDay = Date.parse(@button.date.to_s).strftime("%w")
      @buttonDay = Date.parse(@button.date.to_s).strftime("%d")
      @buttonMonth = Date.parse(@button.date.to_s).strftime("%m")
      @buttonTime = Time.now.strftime("%H:%M")
    end
    case @buttonWeekDay
    when "1"
      @buttonWeekDay = "Lunes"
    when "2"
      @buttonWeekDay = "Martes"
    when "3"
      @buttonWeekDay = "Miércoles"
    when "4"
      @buttonWeekDay = "Jueves"
    when "5"
      @buttonWeekDay = "Viernes"
    when "6"
      @buttonWeekDay = "Sábado"
    when "0"
      @buttonWeekDay = "Domingo"
    else
      @buttonWeekDay = "Domingo"
    end

    case @buttonMonth
    when "1"
      @buttonMonth = "enero"
    when "2"
      @buttonMonth = "febrero"
    when "3"
      @buttonMonth = "marzo"
    when "4"
      @buttonMonth = "abril"
    when "5"
      @buttonMonth = "mayo"
    when "6"
      @buttonMonth = "junio"
    when "7"
      @buttonMonth = "julio"
    when "8"
      @buttonMonth = "agosto"
    when "9"
      @buttonMonth = "septiembre"
    when "10"
      @buttonMonth = "octubre"
    when "11"
      @buttonMonth = "noviembre"
    when "12"
      @buttonMonth = "diciembre"
    else
      @buttonMonth = "diciembre"
    end

    if @button.date == "Ahora"
      @buttonDate = @button.date
    else
      @buttonDate = @buttonWeekDay + ", " + @buttonDay + " de " + @buttonMonth + " a las " + @buttonTime
    end

    @isNeedType = false
    @isOfferType = false
    @isChangeType = false
    case @button.button_type
    when "need"
      @isNeedType = true
    when "offer"
      @isOfferType = true
    when "change"
      @isChangeType = true
    else
      @isChangeType = true
    end

    mail( to: @interested.email, subject: I18n.t('activerecord.mailers.user_notifier.button_tag.subject'))
  end

  def new_chat(user, chat)
    @user = user
    @chat = chat
    @button = @chat.button

    if user.avatar.nil?
      @avatar = nil
    else
      @avatar = user.avatar.asset_url_normal
    end
    if @button.image.nil?
      @buttonImage = nil
    else
      @buttonImage = @button.image.asset_url_normal
    end
    @redirectURL = "https://www.helpbuttons.org/button/" + @button.id.to_s
    if @button.date == "Ahora"
      @buttonDate = Time.now.strftime("%w, %d/%m/%Y")
      @buttonWeekDay = Time.now.strftime("%w")
      @buttonDay = Time.now.strftime("%d")
      @buttonMonth = Time.now.strftime("%m")
      @buttonTime = Time.now.strftime("%H:%M")
    else
      @buttonDate = Date.parse(@button.date.to_s).strftime("%w, %d/%m/%Y")
      @buttonWeekDay = Date.parse(@button.date.to_s).strftime("%w")
      @buttonDay = Date.parse(@button.date.to_s).strftime("%d")
      @buttonMonth = Date.parse(@button.date.to_s).strftime("%m")
      @buttonTime = Time.now.strftime("%H:%M")
    end
    case @buttonWeekDay
    when "1"
      @buttonWeekDay = "Lunes"
    when "2"
      @buttonWeekDay = "Martes"
    when "3"
      @buttonWeekDay = "Miércoles"
    when "4"
      @buttonWeekDay = "Jueves"
    when "5"
      @buttonWeekDay = "Viernes"
    when "6"
      @buttonWeekDay = "Sábado"
    when "0"
      @buttonWeekDay = "Domingo"
    else
      @buttonWeekDay = "Domingo"
    end

    case @buttonMonth
    when "1"
      @buttonMonth = "enero"
    when "2"
      @buttonMonth = "febrero"
    when "3"
      @buttonMonth = "marzo"
    when "4"
      @buttonMonth = "abril"
    when "5"
      @buttonMonth = "mayo"
    when "6"
      @buttonMonth = "junio"
    when "7"
      @buttonMonth = "julio"
    when "8"
      @buttonMonth = "agosto"
    when "9"
      @buttonMonth = "septiembre"
    when "10"
      @buttonMonth = "octubre"
    when "11"
      @buttonMonth = "noviembre"
    when "12"
      @buttonMonth = "diciembre"
    else
      @buttonMonth = "diciembre"
    end

    if @button.date == "Ahora"
      @buttonDate = @button.date
    else
      @buttonDate = @buttonWeekDay + ", " + @buttonDay + " de " + @buttonMonth + " a las " + @buttonTime
    end

    @isNeedType = false
    @isOfferType = false
    @isChangeType = false
    case @button.button_type
    when "need"
      @isNeedType = true
    when "offer"
      @isOfferType = true
    when "change"
      @isChangeType = true
    else
      @isChangeType = true
    end
    if @user.id == @button.creator.id
      @redirectchatURL= "https://www.helpbuttons.org/button/" + @button.id.to_s + "/chat?isButton=true"
    else
      @redirectchatURL= "https://www.helpbuttons.org/button/" + @button.id.to_s + "/chat/" + @chat.id + "?isButton=true"
    end
    @owner = @button.creator
    mail( to: @owner.email, subject: I18n.t('activerecord.mailers.user_notifier.new_chat.subject'))
  end

   def deleted_chat(user, chat, deleted_button)
    @user = user
    @chat = chat
    @button = @chat.button
    @owner = @button.creator
    @deleted_button = deleted_button
    mail( to: @user.email, subject: I18n.t('activerecord.mailers.user_notifier.deleted_chat.subject'))
  end

  def new_message(user, message)
    @sender = user
    @message = message
    @chat = @message.chat
    @button = @chat.button
    @owner = @button.creator
    @send_to_users = @chat.chatters - [@sender]
    if @sender.avatar.nil?
      @avatarSender = nil
    else
      @avatarSender = @sender.avatar.asset_url_normal
    end
    if @owner.avatar.nil?
      @avatar = nil
    else
      @avatar = @owner.avatar.asset_url_normal
    end

    if @button.image.nil?
      @buttonImage = nil
    else
      @buttonImage = @button.image.asset_url_normal
    end
    @redirectURL = "https://www.helpbuttons.org/button/" + @button.id.to_s
    # @redirectchatURL= "https://www.helpbuttons.org/button/" + @button.id.to_s + "/chat?isButton=true"
    if @sender.id == @button.creator.id
      @redirectchatURL= "https://www.helpbuttons.org/profile/button/" + @button.id.to_s + "/chat?id=" + @button.id.to_s + "&isButton=true"
    else
      @redirectchatURL= "https://www.helpbuttons.org/profile/button/" + @button.id.to_s + "/chat/" + @chat.id.to_s + "?" + "id=" + @chat.id.to_s + "&isButton=false"
    end

    if @button.date == "Ahora"
      @buttonDate = Time.current.strftime("%w, %d/%m/%Y")
      @buttonWeekDay = Time.current.strftime("%w")
      @buttonDay = Time.current.strftime("%d")
      @buttonMonth = Time.current.strftime("%m")
      @buttonTime = Time.now.strftime("%H:%M")
    else
      @buttonDate = Date.parse(@button.date.to_s).strftime("%w, %d/%m/%Y")
      @buttonWeekDay = Date.parse(@button.date.to_s).strftime("%w")
      @buttonDay = Date.parse(@button.date.to_s).strftime("%d")
      @buttonMonth = Date.parse(@button.date.to_s).strftime("%m")
      @buttonTime = Time.now.strftime("%H:%M")
    end
    case @buttonWeekDay
    when "1"
      @buttonWeekDay = "Lunes"
    when "2"
      @buttonWeekDay = "Martes"
    when "3"
      @buttonWeekDay = "Miércoles"
    when "4"
      @buttonWeekDay = "Jueves"
    when "5"
      @buttonWeekDay = "Viernes"
    when "6"
      @buttonWeekDay = "Sábado"
    when "0"
      @buttonWeekDay = "Domingo"
    else
      @buttonWeekDay = "Domingo"
    end

    case @buttonMonth
    when "1"
      @buttonMonth = "enero"
    when "2"
      @buttonMonth = "febrero"
    when "3"
      @buttonMonth = "marzo"
    when "4"
      @buttonMonth = "abril"
    when "5"
      @buttonMonth = "mayo"
    when "6"
      @buttonMonth = "junio"
    when "7"
      @buttonMonth = "julio"
    when "8"
      @buttonMonth = "agosto"
    when "9"
      @buttonMonth = "septiembre"
    when "10"
      @buttonMonth = "octubre"
    when "11"
      @buttonMonth = "noviembre"
    when "12"
      @buttonMonth = "diciembre"
    else
      @buttonMonth = "diciembre"
    end

    if @button.date == "Ahora"
      @buttonDate = @button.date
    else
      @buttonDate = @buttonWeekDay + ", " + @buttonDay + " de " + @buttonMonth + " a las " + @buttonTime
    end

    @isNeedType = false
    @isOfferType = false
    @isChangeType = false
    case @button.button_type
    when "need"
      @isNeedType = true
    when "offer"
      @isOfferType = true
    when "change"
      @isChangeType = true
    else
      @isChangeType = true
    end
    unless @send_to_users.blank?
      @send_to_users.each do |user|
        if user.email != @sender.email
          @user = user
          mail( to: user.email, subject: I18n.t('activerecord.mailers.user_notifier.new_message.subject'))
        end
      end
    end

  end

  def transfered_button(user, button)
    @owner = user
    @button = button
    @new_owner=button.creator

    if user.avatar.nil?
      @avatar = nil
    else
      @avatar = user.avatar.asset_url_normal
    end
    if @button.image.nil?
      @buttonImage = nil
    else
      @buttonImage = @button.image.asset_url_normal
    end
    @redirectURL = "https://www.helpbuttons.org/button/" + @button.id.to_s

    if @button.date == "Ahora"
      @buttonDate = Time.now.strftime("%w, %d/%m/%Y")
      @buttonWeekDay = Time.now.strftime("%w")
      @buttonDay = Time.now.strftime("%d")
      @buttonMonth = Time.now.strftime("%m")
      @buttonTime = Time.now.strftime("%H:%M")
    else
      @buttonDate = Date.parse(@button.date.to_s).strftime("%w, %d/%m/%Y")
      @buttonWeekDay = Date.parse(@button.date.to_s).strftime("%w")
      @buttonDay = Date.parse(@button.date.to_s).strftime("%d")
      @buttonMonth = Date.parse(@button.date.to_s).strftime("%m")
      @buttonTime = Time.now.strftime("%H:%M")
    end
    case @buttonWeekDay
    when "1"
      @buttonWeekDay = "Lunes"
    when "2"
      @buttonWeekDay = "Martes"
    when "3"
      @buttonWeekDay = "Miércoles"
    when "4"
      @buttonWeekDay = "Jueves"
    when "5"
      @buttonWeekDay = "Viernes"
    when "6"
      @buttonWeekDay = "Sábado"
    when "0"
      @buttonWeekDay = "Domingo"
    else
      @buttonWeekDay = "Domingo"
    end

    case @buttonMonth
    when "1"
      @buttonMonth = "enero"
    when "2"
      @buttonMonth = "febrero"
    when "3"
      @buttonMonth = "marzo"
    when "4"
      @buttonMonth = "abril"
    when "5"
      @buttonMonth = "mayo"
    when "6"
      @buttonMonth = "junio"
    when "7"
      @buttonMonth = "julio"
    when "8"
      @buttonMonth = "agosto"
    when "9"
      @buttonMonth = "septiembre"
    when "10"
      @buttonMonth = "octubre"
    when "11"
      @buttonMonth = "noviembre"
    when "12"
      @buttonMonth = "diciembre"
    else
      @buttonMonth = "diciembre"
    end

    if @button.date == "Ahora"
      @buttonDate = @button.date
    else
      @buttonDate = @buttonWeekDay + ", " + @buttonDay + " de " + @buttonMonth + " a las " + @buttonTime
    end

    @isNeedType = false
    @isOfferType = false
    @isChangeType = false
    case @button.button_type
    when "need"
      @isNeedType = true
    when "offer"
      @isOfferType = true
    when "change"
      @isChangeType = true
    else
      @isChangeType = true
    end

      mail( to: @new_owner.email, subject: I18n.t('activerecord.mailers.user_notifier.transfered_button.subject'))

  end

  def transfered_noreg_button(user, button, email)
    @owner = user
    @button = button
    @email = email
    @new_owner=button.creator

    if user.avatar.nil?
      @avatar = nil
    else
      @avatar = user.avatar.asset_url_normal
    end
    if @button.image.nil?
      @buttonImage = nil
    else
      @buttonImage = @button.image.asset_url_normal
    end
    @redirectURL = "https://www.helpbuttons.org/register/?email=" + email

    if @button.date == "Ahora"
      @buttonDate = Time.now.strftime("%w, %d/%m/%Y")
      @buttonWeekDay = Time.now.strftime("%w")
      @buttonDay = Time.now.strftime("%d")
      @buttonMonth = Time.now.strftime("%m")
      @buttonTime = Time.now.strftime("%H:%M")
    else
      @buttonDate = Date.parse(@button.date.to_s).strftime("%w, %d/%m/%Y")
      @buttonWeekDay = Date.parse(@button.date.to_s).strftime("%w")
      @buttonDay = Date.parse(@button.date.to_s).strftime("%d")
      @buttonMonth = Date.parse(@button.date.to_s).strftime("%m")
      @buttonTime = Time.now.strftime("%H:%M")
    end
    case @buttonWeekDay
    when "1"
      @buttonWeekDay = "Lunes"
    when "2"
      @buttonWeekDay = "Martes"
    when "3"
      @buttonWeekDay = "Miércoles"
    when "4"
      @buttonWeekDay = "Jueves"
    when "5"
      @buttonWeekDay = "Viernes"
    when "6"
      @buttonWeekDay = "Sábado"
    when "0"
      @buttonWeekDay = "Domingo"
    else
      @buttonWeekDay = "Domingo"
    end

    case @buttonMonth
    when "1"
      @buttonMonth = "enero"
    when "2"
      @buttonMonth = "febrero"
    when "3"
      @buttonMonth = "marzo"
    when "4"
      @buttonMonth = "abril"
    when "5"
      @buttonMonth = "mayo"
    when "6"
      @buttonMonth = "junio"
    when "7"
      @buttonMonth = "julio"
    when "8"
      @buttonMonth = "agosto"
    when "9"
      @buttonMonth = "septiembre"
    when "10"
      @buttonMonth = "octubre"
    when "11"
      @buttonMonth = "noviembre"
    when "12"
      @buttonMonth = "diciembre"
    else
      @buttonMonth = "diciembre"
    end

    if @button.date == "Ahora"
      @buttonDate = @button.date
    else
      @buttonDate = @buttonWeekDay + ", " + @buttonDay + " de " + @buttonMonth + " a las " + @buttonTime
    end

    @isNeedType = false
    @isOfferType = false
    @isChangeType = false
    case @button.button_type
    when "need"
      @isNeedType = true
    when "offer"
      @isOfferType = true
    when "change"
      @isChangeType = true
    else
      @isChangeType = true
    end

      mail( to: @email, subject: I18n.t('activerecord.mailers.user_notifier.transfered_button.subject'))

  end

  def outdated_button(user, button)
    @user = user
    @button = button
    @new_owner=button.creator

    if user.avatar.nil?
      @avatar = nil
    else
      @avatar = user.avatar.asset_url_normal
    end
    if @button.image.nil?
      @buttonImage = nil
    else
      @buttonImage = @button.image.asset_url_normal
    end
    @redirectURL = "https://www.helpbuttons.org/button/" + @button.id.to_s
    if @button.date == "Ahora"
      @buttonDate = Time.now.strftime("%w, %d/%m/%Y")
      @buttonWeekDay = Time.now.strftime("%w")
      @buttonDay = Time.now.strftime("%d")
      @buttonMonth = Time.now.strftime("%m")
      @buttonTime = Time.now.strftime("%H:%M")
    else
      @buttonDate = Date.parse(@button.date.to_s).strftime("%w, %d/%m/%Y")
      @buttonWeekDay = Date.parse(@button.date.to_s).strftime("%w")
      @buttonDay = Date.parse(@button.date.to_s).strftime("%d")
      @buttonMonth = Date.parse(@button.date.to_s).strftime("%m")
      @buttonTime = Time.now.strftime("%H:%M")
    end
    case @buttonWeekDay
    when "1"
      @buttonWeekDay = "Lunes"
    when "2"
      @buttonWeekDay = "Martes"
    when "3"
      @buttonWeekDay = "Miércoles"
    when "4"
      @buttonWeekDay = "Jueves"
    when "5"
      @buttonWeekDay = "Viernes"
    when "6"
      @buttonWeekDay = "Sábado"
    when "0"
      @buttonWeekDay = "Domingo"
    else
      @buttonWeekDay = "Domingo"
    end

    case @buttonMonth
    when "1"
      @buttonMonth = "enero"
    when "2"
      @buttonMonth = "febrero"
    when "3"
      @buttonMonth = "marzo"
    when "4"
      @buttonMonth = "abril"
    when "5"
      @buttonMonth = "mayo"
    when "6"
      @buttonMonth = "junio"
    when "7"
      @buttonMonth = "julio"
    when "8"
      @buttonMonth = "agosto"
    when "9"
      @buttonMonth = "septiembre"
    when "10"
      @buttonMonth = "octubre"
    when "11"
      @buttonMonth = "noviembre"
    when "12"
      @buttonMonth = "diciembre"
    else
      @buttonMonth = "diciembre"
    end


    if @button.date == "Ahora"
      @buttonDate = @button.date
    else
      @buttonDate = @buttonWeekDay + ", " + @buttonDay + " de " + @buttonMonth + " a las " + @buttonTime
    end

    @isNeedType = false
    @isOfferType = false
    @isChangeType = false
    case @button.button_type
    when "need"
      @isNeedType = true
    when "offer"
      @isOfferType = true
    when "change"
      @isChangeType = true
    else
      @isChangeType = true
    end

      mail( to: @user.email, subject: I18n.t('activerecord.mailers.user_notifier.transfered_button.subject'))

  end

  def new_button_net(user, button_net)
    @user = user
    if @user.active
      if user.avatar.nil?
        @avatar = nil
      else
        @avatar = user.avatar.asset_url_normal
      end

      @button_net = button_net

      if @button_net.img_url
        @button_netImage = nil
      else
        @button_netImage = @button_net.img_url
      end

      sendMail = true

      @redirectURL = "https://www.helpbuttons.org/" + @button_net.name

      # mail( to: @user.email, subject: I18n.t('activerecord.mailers.user_notifier.new_button.subject'))

      # Users.super_admin.each do |user|
      #     @user = user
      #     mail( to: user.email, subject: I18n.t('activerecord.mailers.user_notifier.new_button_net.subject'))
      # end
    end
  end

  def accepted_button_net(user, button_net)
    @user = user
    if @user.active
      if user.avatar.nil?
        @avatar = nil
      else
        @avatar = user.avatar.asset_url_normal
      end
      @button_net = button_net
      if @button_net.img_url
        @button_netImage = nil
      else
        @button_netImage = @button_net.img_url
      end
      if @user.id != @button_net.creator.id
      end
      sendMail = true

      @redirectURL = "https://www.helpbuttons.org/button/" + @button_net.net_url

      mail( to: @user.email, subject: I18n.t('activerecord.mailers.user_notifier.new_button.subject'))
    end
  end

  def accepted_button_net(user, button_net)
    @user = user
    if @user.active
      if user.avatar.nil?
        @avatar = nil
      else
        @avatar = user.avatar.asset_url_normal
      end
      @button_net = button_net
      if @button_net.img_url
        @button_netImage = nil
      else
        @button_netImage = @button_net.img_url
      end
      if @user.id != @button_net.creator.id
      end
      sendMail = true

      @redirectURL = "https://www.helpbuttons.org/button/" + @button_net.net_url

      mail( to: @user.email, subject: I18n.t('activerecord.mailers.user_notifier.new_button.subject'))
    end
  end

end
