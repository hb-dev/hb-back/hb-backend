class Chats::MessagesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_chat

  def index
    @chat.messages.each do |message|
      if current_user.id != message.user_id
        message.update(read: true)
      end
    end

    render json: @chat.messages,
           each_serializer: Message::ShowSerializer
  end

  private

  def set_chat
    @chat = Resources::Get.exec params
    respond_with_not_found(Chat.name) if @chat.nil?
  end
end
