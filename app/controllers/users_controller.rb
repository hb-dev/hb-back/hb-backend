#     <Helpbuttons, collaborative network-making software>
#     Copyright (C) 2013-2121  Helpbuttons
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
class UsersController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :set_user, except: [:index]

  rescue_from ActiveRecord::RecordNotFound do
    head :not_found, location: user_path(params[:id])
  end

  def index
    @userResult = User.find_by(email: params[:email]);
    if params[:actionEvent]  == 'button_transfered' && @userResult.nil?
      @buttonTransfered = Button.find(params[:button_id])
      if !@buttonTransfered.nil?
        @transfered = TransferedButton.find_by(button_id: @buttonTransfered.id)
        if @transfered.nil?
          TransferedButton.create(button_id: @buttonTransfered.id, email: params[:email], user_id: current_user.id)
        else
          @transfered.update(email: params[:email])
        end
        UserNotifierMailer.transfered_noreg_button(current_user, @buttonTransfered, params[:email]).deliver
        respond_with_not_found("User");
      end
    else
      render json: @userResult,
             include: include_for(@userResult),
             serializer: serializer_for(@userResult)
    end
  end

  def show
    render json: @user,
           include: include_for(@user),
           serializer: serializer_for(@user)
  end

  def update
    render_not_found and return if @current_user.blank? && @user === @current_user
    if params[:data][:attributes][:'action-event'] === 'unblock'
      arr = @user.blocked - params[:data][:attributes][:'blocked']
      arr = User.where(id: arr)
      arr.each do |user|
        user.update(blocked_by: user.blocked_by - [current_user.id])
      end
    end
    user = Users::Update.exec params, @user
    if user.save
      Users::UpdateOwnedButtons.exec(user)
      # UserNotifierMailer.updated_user(user).deliver
      render json: user,
             include: include_for(@user),
             serializer: User::LoginSerializer
    else
      respond_with_errors(user)
    end
  end

  def update_likes
    @otherUser = User.find(params[:data][:attributes][:"other-user-id"]);
    render_not_found and return if @current_user.blank? && @otherUser === @current_user
    if @current_user.liked.find_index(@otherUser.id).nil?
      @otherUser.likes = @otherUser.likes + 1
      @current_user.liked.push(@otherUser.id);
    else
      @otherUser.likes = @otherUser.likes - 1
      @current_user.liked.delete_at(@current_user.liked.find_index(@otherUser.id))
    end
    if @otherUser.save and @current_user.save
      render json: @current_user,
             serializer: User::LoginSerializer
    else
      respond_with_errors(user)
    end
  end

  def update_blocked_by
    @otherUser = User.find(params[:data][:attributes][:"other-user-id"]);
    render_not_found and return if @current_user.blank? && @otherUser === @current_user
    if @otherUser.blocked_by.find_index(@current_user.id).nil?
      @otherUser.blocked_by.push(@current_user.id)
      @current_user.blocked.push(@otherUser.id)
    else
      @otherUser.blocked_by.delete_at(@otherUser.blocked_by.find_index(@current_user.id));
      @current_user.blocked.delete_at(@current_user.blocked.find_index(@otherUser.id))
    end
    if @otherUser.save and @current_user.save
      render json: @current_user,
             serializer: User::LoginSerializer
    else
      respond_with_errors(user)
    end
  end

  def password
    respond_with_unauthorized if current_user.blank? || @user != current_user
    user = Users::CheckPassword.exec params, @user
    if user.errors.blank?
      user = Users::ChangePassword.exec params, @user
      if user.save
        render json: user,
        include: include_for(@user),
        serializer: User::LoginSerializer
      else
        respond_with_errors(user)
      end
    else
      respond_with_errors(user)
    end

  end

  def remove_profile
    render_not_found and return if @current_user.blank? && @user === @current_user
    if @current_user.destroy
      render json: { data: [] }
    else
      respond_with_errors("Error al eliminar usuario");
    end
  end

  protected

  def serializer_for user
    if params[:id] == 'current'
      User::LoginSerializer
    else
      User::ShowSerializer
    end
  end

  def transfer_button_to user, user_to, button
      if user_to.exists?(user_to.nickname)
        button.creator = user_to
        user_to.update
        user.update
      # your truck exists in the database
      else
        respond_with_not_found(User.name)
      end
  end

  def include_for user
    if params[:id] == 'current'
      ['avatar', 'chats']
    else
      ['avatar']
    end
  end

  def set_user
    @user = params[:id] == 'current' ? current_user : Resources::Get.exec(params)
    respond_with_not_found(User.name) if @user.nil?
  end
end
