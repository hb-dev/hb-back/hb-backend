#     <Helpbuttons, collaborative network-making software>
#     Copyright (C) 2013-2121  Helpbuttons
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
class Buttons::ButtonNetsController < ApplicationController
  before_action :set_button

  def add_button_nets
    # respond_with_unauthorized and return if current_user.blank? || current_user != @button.creator
      new_button_net = ButtonNet::Retrieve.exec params
      @button.button_net << (new_button_net - @button.button_net)
    respond_with_ok
  end

  def remove_button_nets
    # respond_with_unauthorized and return if current_user.blank? || current_user != @button.creator
      old_button_net = ButtonNet::Retrieve.exec params
      @button.button_net.destroy(old_button_net - (old_button_net - @button.button_net))
    respond_with_ok
  end

  def update_button_nets
    # respond_with_unauthorized and return if current_user.blank? || current_user != @button.creator
      updated_button_net = ButtonNet::Retrieve.exec params
      @button.button_net.clear
      @button.button_net << updated_button_net
    respond_with_ok
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_button
    @button = Resources::Get.exec params
    respond_with_not_found(Button.name) if @button.nil?
  end

end
