#     <Helpbuttons, collaborative network-making software>
#     Copyright (C) 2013-2121  Helpbuttons
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
class Buttons::ChatsController < ApplicationController
  before_action :set_button, only: [:sharer_chats, :sharer_chats_relation]

  def sharer_chats
    chats = get_button_chats
    unless chats.blank?
      render json: chats,
             include: ['button_creator.*','user.*','button.*'],
             current_user: current_user,
             each_serializer: Chat::ShowSerializer
    else
      respond_with_empty
    end
  end

  def sharer_chats_relation
    chats = get_button_chats
    unless chats.blank?
      render json: chats,
             current_user: current_user,
             each_serializer: Chat::NestedIndexSerializer
    else
      respond_with_empty
    end
  end

  private

  def get_button_chats
    return [] if current_user.blank?
    current_user==@button.creator ? @button.chats : @button.chats.sharer?(current_user)
  end

  def set_button
    @button = Resources::Get.exec params
    respond_with_not_found(Button.name) if @button.nil?
  end

end
