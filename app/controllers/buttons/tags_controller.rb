#     <Helpbuttons, collaborative network-making software>
#     Copyright (C) 2013-2121  Helpbuttons
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
class Buttons::TagsController < ApplicationController
  before_action :set_button

  def add_offer_tags
    # respond_with_unauthorized and return if current_user.blank? || current_user != @button.creator
    if @button.button_type=="offer" || @button.button_type=="change"
      new_offer_tags = Tags::Retrieve.exec params
      @button.offer_tags << (new_offer_tags - @button.offer_tags)
    end
    respond_with_ok
  end

  def remove_offer_tags
    # respond_with_unauthorized and return if current_user.blank? || current_user != @button.creator
    if @button.button_type=="offer" || @button.button_type=="change"
      old_offer_tags = Tags::Retrieve.exec params
      @button.offer_tags.destroy(old_offer_tags - (old_offer_tags - @button.offer_tags))
    end
    respond_with_ok
  end

  def update_offer_tags
    # respond_with_unauthorized and return if current_user.blank? || current_user != @button.creator
    if @button.button_type=="offer" || @button.button_type=="change"
      updated_offer_tags = Tags::Retrieve.exec params
      @button.offer_tags.clear
      @button.offer_tags << updated_offer_tags
    end
    respond_with_ok
  end

  def add_needed_tags
    # respond_with_unauthorized and return if current_user.blank? || current_user != @button.creator
    if @button.button_type=="need" || @button.button_type=="change"
      new_needed_tags = Tags::Retrieve.exec params
      @button.needed_tags << (new_needed_tags - @button.needed_tags)
    end
    respond_with_ok
  end

  def remove_needed_tags
    # respond_with_unauthorized and return if current_user.blank? || current_user != @button.creator
    if @button.button_type=="need" || @button.button_type=="change"
      old_needed_tags = Tags::Retrieve.exec params
      @button.needed_tags.destroy(old_needed_tags - (old_needed_tags - @button.needed_tags))
    end
    respond_with_ok
  end

  def update_needed_tags
    # respond_with_unauthorized and return if current_user.blank? || current_user != @button.creator
    if @button.button_type=="need" || @button.button_type=="change"
      updated_needed_tags = Tags::Retrieve.exec params
      @button.needed_tags.clear
      @button.needed_tags << updated_needed_tags
    end
    respond_with_ok
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_button
    @button = Resources::Get.exec params
    respond_with_not_found(Button.name) if @button.nil?
  end

end
