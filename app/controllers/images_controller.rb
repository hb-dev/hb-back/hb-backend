class ImagesController < ApplicationController
  before_action :set_image, only: [:show, :destroy]

  def create
    image = Images::Create.exec params
    if image.save
      # button = image.imageable
      # check_mailer = if button.active == true && button.active_was == false
      #                 true
      #               elsif button.active == false && button.active_was == true
      #                 false
      #               else
      #                 nil
      #               end
      # if button != nil && image.imageable_type == "Button"
        # UserNotifierMailer.updated_button(button.creator, button, check_mailer).deliver
      # end

      render json: image, serializer: Image::ShowSerializer
    else
      respond_with_errors(image)
    end
  end

  def show
    render json: @image, serializer: Image::ShowSerializer
  end

  def destroy
  if @image.destroy
    respond_with_ok
  else
    respond_with_errors @image
  end
end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_image
    @image = Resources::Get.exec params
    respond_with_not_found(Image.name) if @image.nil?
  end

  def image_params
    {
      imageable_id: params['imageable-id'],
      imageable_type: params['imageable-type'].try(:capitalize).try(:singularize),
      image_property: params['imageable-property'],
      file: params[:file]
    }
  end
end
