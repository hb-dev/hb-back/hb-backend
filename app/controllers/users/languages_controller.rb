class Users::LanguagesController < ApplicationController
  before_action :authenticate_user!

  def update_languages
    # respond_with_unauthorized and return if current_user.blank? 
    updated_languages = Languages::Retrieve.exec params
    current_user.languages.clear
    current_user.languages << updated_languages
    respond_with_ok
  end

end
