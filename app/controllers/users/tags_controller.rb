class Users::TagsController < ApplicationController
  before_action :authenticate_user!

  def update_interests
    # respond_with_unauthorized and return if current_user.blank?
    updated_interests = Tags::Retrieve.exec params
    current_user.user_tags.clear
    current_user.user_tags << updated_interests
    respond_with_ok
  end

end
