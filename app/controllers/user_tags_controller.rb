#     <Helpbuttons, collaborative network-making software>
#     Copyright (C) 2013-2121  Helpbuttons
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
class UserTagsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :destroy, :update]
  before_action :set_tag, only: [:show, :destroy, :update]

  def index
    tags = UserTags::Query.exec params
    render json: tags,
           each_serializer: UserTag::IndexSerializer
  end

  def create
    tag = UserTags::Create.exec params
    # tag.search_name = I18n.transliterate(tag.name.downcase)
    tag.update(user_id: current_user.id)
    if tag.save
      render json: tag,
             serializer: UserTag::ShowSerializer
    else
      respond_with_errors(tag)
    end
  end

  def show
    render json: @tag,
           serializer: UserTag::ShowSerializer
  end

  def update
    respond_with_not_found and return if @tag.blank?
    respond_with_unauthorized and return if @tag.user != current_user
    tag = UserTags::Update.exec params, @tag
    if tag.save
      render json: tag,
             serializer: UserTag::ShowSerializer
    else
      respond_with_errors(tag)
    end
  end

  def destroy
    if @tag.destroy
      respond_with_ok
    else
      respond_with_errors(@tag)
    end
  end

  private

  def set_tag
    @tag = Resources::Get.exec params
    respond_with_not_found(UserTag.name) if @tag.nil?
  end

end
