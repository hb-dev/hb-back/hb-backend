#     <Helpbuttons, collaborative network-making software>
#     Copyright (C) 2013-2121  Helpbuttons
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
class ChatsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_chat, only: [:show, :destroy]

  def index
    chats = Chats::Query.exec params, current_user
    if chats
      render json: chats,
             include: include_for_chat,
             current_user: current_user,
             each_serializer: Chat::ShowSerializer
    else
      respond_with_empty
    end
  end

  def create
    chat = Chats::Create.exec params, current_user
    if chat.save
      # UserNotifierMailer.new_chat(current_user, chat).deliver
      render json: chat,
             include: include_for_chat,
             current_user: current_user,
             serializer: Chat::ShowSerializer
    else
      respond_with_errors(chat)
    end
  end

  def show
    respond_with_unauthorized and return if current_user.nil? || (current_user!=@chat.button_creator && current_user!=@chat.user)
    render json: @chat,
           include: include_for_chat,
           current_user: current_user,
           serializer: Chat::ShowSerializer
  end

  def destroy
    respond_with_unauthorized and return if current_user.nil? || (current_user!=@chat.button_creator)
    if Chats::Delete.exec @chat, current_user
      respond_with_ok
    else
      respond_with_errors(@chat)
    end
  end

  private

  def set_chat
    @chat = Resources::Get.exec params
    respond_with_not_found(Chat.name) if @chat.nil?
  end

  def include_for_chat
    ['button_creator.*','user.*','button.*']
  end

end
