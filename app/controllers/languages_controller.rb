class LanguagesController < ApplicationController
  before_action :set_tag, only: [:show]

  def index
    languages = Languages::Query.exec params
    render json: languages,
           each_serializer: Language::IndexSerializer
  end

  def show
    render json: @language,
           serializer: Language::ShowSerializer
  end

  private

  def set_tag
    @language = Resources::Get.exec params
    respond_with_not_found(Language.name) if @language.nil?
  end

end
