class TagsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_tag, only: [:show, :destroy]

  def index
    tags = Tags::Query.exec params
    render json: tags,
           each_serializer: Tag::IndexSerializer
  end

  def create
    tag = Tags::Create.exec params
    tag.search_name = I18n.transliterate(tag.name.downcase)

    if tag.save
      render json: tag,
             serializer: Tag::ShowSerializer
    else
      respond_with_errors(tag)
    end
  end

  def show
    render json: @tag,
           serializer: Tag::ShowSerializer
  end

  def destroy
    if @tag.destroy
      respond_with_ok
    else
      respond_with_errors(@tag)
    end
  end

  private

  def set_tag
    @tag = Resources::Get.exec params
    respond_with_not_found(Tag.name) if @tag.nil?
  end

end
