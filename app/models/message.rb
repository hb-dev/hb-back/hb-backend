# ## Schema Information
#
# Table name: `messages`
#
# ### Columns
#
# Name              | Type               | Attributes
# ----------------- | ------------------ | ---------------------------
# **`body`**        | `text`             |
# **`chat_id`**     | `integer`          |
# **`created_at`**  | `datetime`         | `not null`
# **`id`**          | `integer`          | `not null, primary key`
# **`read`**        | `boolean`          | `default(FALSE)`
# **`updated_at`**  | `datetime`         | `not null`
# **`user_id`**     | `integer`          |
#
# ### Indexes
#
# * `index_messages_on_chat_id`:
#     * **`chat_id`**
# * `index_messages_on_user_id`:
#     * **`user_id`**
#
# ### Foreign Keys
#
# * `fk_rails_...`:
#     * **`chat_id => chats.id`**
# * `fk_rails_...`:
#     * **`user_id => users.id`**
#

class Message < ApplicationRecord
  belongs_to :user
  belongs_to :chat

  scope :unread_by_user, ->(user=nil) { unless user.nil?
      where 'user_id != ? AND read IS FALSE', user
    else
      []
    end }

end
