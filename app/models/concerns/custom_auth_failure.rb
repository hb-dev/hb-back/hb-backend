class CustomAuthFailure < Devise::FailureApp
  def respond
    self.status = 401
    self.content_type = 'json'
    self.response_body = {"errors" => [title: I18n.t("activerecord.errors.login.user_password_error"), detail: I18n.t("activerecord.errors.login.user_password_error_description")]}.to_json
  end
end
