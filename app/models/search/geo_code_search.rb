class Search::GeoCodeSearch
  include ActiveModel::Model
  include Geokit::Geocoders

  SEARCH_TYPES = {area:'area',point:'point',all:'all',id:'ids',fromTo:'fromTo'}

  def self.search_type(search_params)
    type = nil
    type = if self.area_criteria(search_params)
             SEARCH_TYPES[:area]
           elsif (search_params[:lat].present? and search_params[:lng].present?) || search_params[:adr].present?
             SEARCH_TYPES[:point]
           elsif search_params[:id].present?
             SEARCH_TYPES[:id]
           elsif self.fromTo_criteria(search_params)
             SEARCH_TYPES[:fromTo]
           else
             SEARCH_TYPES[:all]
           end
    type
  end

  def self.get_searcher(search_type,search_params)
    seeker = if search_type == SEARCH_TYPES[:area]
               Search::AreaSearch.new(search_params)
             elsif search_type == SEARCH_TYPES[:point]
               Search::PointSearch.new(search_params)
             elsif search_type == SEARCH_TYPES[:id]
               Search::SelectedSearch.new(search_params)
             elsif search_type == SEARCH_TYPES[:all]
               Search::CompleteSearch.new(search_params)
             elsif search_type == SEARCH_TYPES[:fromTo]
               Search::AreaSearch.new(search_params)
             end
    seeker
  end

  def initialize(search_params)
    @params = search_params
  end

  def search_buttons
    #abstract method to be implemented in the child classes
  end

  def self.area_criteria(search_params)
    [search_params[:swl].present?,
     search_params[:swlng].present?,
     search_params[:nel].present?,
     search_params[:nelng].present?
    ].all?
  end

  def self.fromTo_criteria(search_params)
    [search_params[:swl].present?,
     search_params[:swlng].present?,
     search_params[:nel].present?,
     search_params[:nelng].present?,
     search_params[:radius].present?
    ].all?
  end

  def meta_info (location,search_params)
    if !(search_params[:nel].blank? || search_params[:nelng].blank? || search_params[:swl].blank? || search_params[:swlng].blank?)
      @swl = search_params[:swl].to_f
      @swlng = search_params[:swlng].to_f
      @nel = search_params[:nel].to_f
      @nelng = search_params[:nelng].to_f
      if !search_params[:total_pages].nil?
        @total_pages = search_params[:total_pages].to_i
      else
        @total_pages = 1
      end
      @center_lat = (@nel.to_f-@swl.to_f)/2+@swl.to_f
      @center_lng = (@nelng.to_f-@swlng.to_f)/2+@swlng.to_f
    else
      @center_lat = location.lat
      @center_lng = location.lng
      @swl = location.suggested_bounds.sw.lat
      @swlng = location.suggested_bounds.sw.lng
      @nel = location.suggested_bounds.ne.lat
      @nelng = location.suggested_bounds.ne.lng
    end
    { swl:@swl, swlng:@swlng, nel:@nel, nelng:@nelng, lat: @center_lat, lng: @center_lng, total_pages: @total_pages }
  end

end
