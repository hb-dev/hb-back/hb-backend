class Search::PointSearch < Search::GeoCodeSearch
  def initialize(search_params)
    super(search_params)
    @address = @params[:adr]
    @address2 = @params[:adr2]
    @lat = @params[:lat]
    @lng = @params[:lng]
    @sw = nil
    @ne = nil
    @buttonNet = @params[:buttonNet]
  end

  def search_buttons
    results, location  = if @address.present?
                           search_by_address
                         elsif @lat.present? && @lng.present?
                           search_by_lat_long
                         end
   # results2, location2  = if @address2.present?
   #                        search_by_address
   #                      end
    meta_info = location.success? ?  meta_info(location,@params) : nil
    results = [] if results.blank?
    return results,meta_info
  end

  def search_by_address
    loc = GoogleGeocoder.geocode(@address)
    if loc.success
       return search_by_possible_level(loc), loc
    else
      return [], loc
    end
  end

  def set_lat_and_lng_by_address
    unless @address.blank?
      if @lat.blank? || @lng.blank?
        if location.success && !location.street_address.nil?
          @address_lat = location.lat
          @address_lng = location.lng
        else
          errors.add(:address, I18n.t("activerecord.errors.search.not_valid_address"))
        end
      end
    end
  end

  def search_by_lat_long
    lat_lng = "#{@lat},#{@lng}"
    loc =  GoogleGeocoder.reverse_geocode lat_lng
    return search_by_possible_level(loc), loc
  end

  def set_lat_lng(loc)
    if loc.success and loc.suggested_bounds.present?
      @sw = Geokit::LatLng.new(loc.suggested_bounds.sw.lat ,loc.suggested_bounds.sw.lng)
      @ne = Geokit::LatLng.new(loc.suggested_bounds.ne.lat ,loc.suggested_bounds.ne.lng)
    end
  end

  def search_by_possible_level(loc)
    return self if !check_success_loc(loc)
    buttons = []
    return buttons if loc.suggested_bounds.blank?
    #Default bounds
    buttons = try_search(loc)
    return buttons if buttons.present?
    #Expand bounds to zip code
    buttons = zip_search(loc)
    return buttons if buttons.present?
    #Expand bounds to city code
    buttons = city_search(loc)
    return buttons if buttons.present?
    #Expand bounds to district code
    buttons = district_search(loc)
    return buttons if buttons.present?
    buttons
  end

  def check_success_loc(location)
    self.errors.add(:geocode,I18n.t('activerecord.errors.search.not_completed_geocoding')) and return false if !location.success
    return true
  end

  def try_search(loc)
    set_lat_lng(loc)
    results = Button.in_bounds([@sw, @ne])
    results
  end

  def zip_search(loc)
    zip_location = (loc.zip.present? && loc.country.present? ? GoogleGeocoder.geocode(loc.zip+","+loc.country) : nil)
    result = (zip_location.blank? ? zip_location : try_search(zip_location))
    result
  end

  def city_search(loc)
    city_location = (loc.city.present? ? GoogleGeocoder.geocode(loc.city) : nil)
    result = (city_location.blank? ? city_location : try_search(city_location))
    result
  end

  def district_search(loc)
    district_location = (loc.district.present? ? GoogleGeocoder.geocode(loc.district + "province") : nil)
    result = (district_location.blank? ? district_location : try_search(district_location))
    result
  end

end
