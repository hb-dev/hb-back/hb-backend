class Search::AreaSearch < Search::GeoCodeSearch

  def initialize(search_params)
    super(search_params)
    @swl = @params[:swl]
    @swlng = @params[:swlng]
    @nel = @params[:nel]
    @nelng = @params[:nelng]
    @radius = @params[:radius]
    @buttonNet = @params[:buttonNet]
    @page = @params[:page]
  end

  def search_buttons
    if @radius
      @sw = Geokit::LatLng.new(@swl, @swlng)
      bounds = Geokit::Bounds.from_point_and_radius(@sw,param* 0.62137)
      result1 = Button.in_bounds(bounds).all
      if @page
        buttons = Button.page(@page)
        @params[:total_pages] = Button.count/5
        return buttons.in_bounds(bounds), meta_info(nil,@params)
      else
        return Button.in_bounds(bounds), meta_info(nil,@params)
      end
    else
      @sw = Geokit::LatLng.new(@swl,@swlng)
      @ne = Geokit::LatLng.new(@nel,@nelng)
      if @page
        buttons = Button.in_bounds([@sw, @ne]).page(@page).per_page(15)
        @params[:total_pages] = Button.in_bounds([@sw, @ne]).count/15
        return buttons, meta_info(nil,@params)
      else
        return Button.in_bounds([@sw, @ne]), meta_info(nil,@params)
      end
    end

  end
end
