class Search::GlobalSearch
  include ActiveModel::Model
  require 'will_paginate/array'

  SEARCH_TYPE = {filter: 'filter',all:'all'}

  attr_accessor :successful_search
  attr_accessor :results

  def self.search(model, params)
    search_type = GlobalSearch.search_type(eval(model).name,params)
    seeker = GlobalSearch.get_seeker(eval(model).name,search_type,params)
    seeker.execute_search(eval(model).name)
    seeker
  end

  def initialize(params)
    @params = params
    @default_page_size = 15
    @default_page = 1
    @page = params[:page].try(:[],:number).blank? ? @default_page : params[:page].try(:[],:number)
    @size = params[:page].try(:[],:size).blank? ? @default_page_size : params[:page].try(:[],:size)
  end

  def self.search_type(model,params)
    eval("#{model}Search").filter_search(params) ? SEARCH_TYPE[:filter] : SEARCH_TYPE[:all]
  end

  def self.get_seeker(model,search_type,params)
    SEARCH_TYPE[:filter] == search_type ? eval("#{model}Search").new(params) : GlobalSearchAll.new(model,params)
  end

  def execute_search_based(model, search_params)
    resources = eval(model).all
    search_params.each do |param|
      resources = resources.where("LOWER(cast(#{param.first.to_s} as text)) ILIKE ?", "%#{eval(model).send(:sanitize_sql_like, param.second.to_s.downcase)}%")
      break if resources.blank?
    end
    resources
  end

  def self.filter_search(params)
    #Method to be implement by the specific seeker
  end

  def execute_search(model)
    #Method to be implement by the specific seeker
  end

  def valid_pag_params?
    [@params[:page][:number].present?, @params[:page][:size].present?, @params[:page][:number].is_a?(Integer), @params[:page][:size].is_a?(Integer)].all?
  end

  def paginate_results(model, results)
    p_results = results.present? ? results.paginate(:page => @page, :per_page => @size) : eval(model).where(id:nil).paginate(:page => @page, :per_page => @size)
  end

end
