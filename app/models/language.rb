# ## Schema Information
#
# Table name: `languages`
#
# ### Columns
#
# Name              | Type               | Attributes
# ----------------- | ------------------ | ---------------------------
# **`code`**        | `string`           |
# **`created_at`**  | `datetime`         | `not null`
# **`id`**          | `integer`          | `not null, primary key`
# **`name`**        | `string`           |
# **`updated_at`**  | `datetime`         | `not null`
#

class Language < ApplicationRecord
    has_and_belongs_to_many :users

    def translated_name
        I18n.t("activerecord.attributes.language.#{code}", default: name)
    end
end
