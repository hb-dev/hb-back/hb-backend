#     <Helpbuttons, collaborative network-making software>
#     Copyright (C) 2013-2121  Helpbuttons
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# ## Schema Information
#
# Table name: `user_tags`
#
# ### Columns
#
# Name               | Type               | Attributes
# ------------------ | ------------------ | ---------------------------
# **`button_nets`**  | `integer`          | `default([]), is an Array`
# **`created_at`**   | `datetime`         | `not null`
# **`id`**           | `integer`          | `not null, primary key`
# **`tag_id`**       | `integer`          |
# **`tag_type`**     | `integer`          |
# **`updated_at`**   | `datetime`         | `not null`
# **`user_id`**      | `integer`          |
#
# ### Indexes
#
# * `index_user_tags_on_tag_id`:
#     * **`tag_id`**
# * `index_user_tags_on_user_id`:
#     * **`user_id`**
#

class UserTag < ApplicationRecord
  belongs_to :user
  belongs_to :tag

  scope :interested, -> { where(tag_type: 2) }

  # after_commit :update_tag_counter
  #
  # def update_tag_counter
  #   self.tag.update_counter
  # end

end
