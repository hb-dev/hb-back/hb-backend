#     <Helpbuttons, collaborative network-making software>
#     Copyright (C) 2013-2121  Helpbuttons
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# ## Schema Information
#
# Table name: `button_tags`
#
# ### Columns
#
# Name              | Type               | Attributes
# ----------------- | ------------------ | ---------------------------
# **`button_id`**   | `integer`          |
# **`created_at`**  | `datetime`         | `not null`
# **`id`**          | `integer`          | `not null, primary key`
# **`tag_id`**      | `integer`          |
# **`tag_type`**    | `integer`          |
# **`updated_at`**  | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_button_tags_on_button_id`:
#     * **`button_id`**
# * `index_button_tags_on_tag_id`:
#     * **`tag_id`**
#

class ButtonTag < ApplicationRecord
  belongs_to :button
  belongs_to :tag

  scope :offered, -> { where(tag_type: 0) }
  scope :needed, -> { where(tag_type: 1) }

  after_commit :update_tag_counter

  def update_tag_counter
    self.tag.update_counter
  end

end
