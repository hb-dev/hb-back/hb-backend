#     <Helpbuttons, collaborative network-making software>
#     Copyright (C) 2013-2121  Helpbuttons
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# ## Schema Information
#
# Table name: `button_nets`
#
# ### Columns
#
# Name                  | Type               | Attributes
# --------------------- | ------------------ | ---------------------------
# **`active`**          | `boolean`          | `default(FALSE)`
# **`admin_users`**     | `integer`          | `default([]), is an Array`
# **`allowed_users`**   | `integer`          | `default([]), is an Array`
# **`blocked_users`**   | `integer`          | `default([]), is an Array`
# **`created_at`**      | `datetime`         | `not null`
# **`creator_id`**      | `integer`          | `not null`
# **`description`**     | `text`             | `not null`
# **`friend_nets_id`**  | `integer`          | `default([]), is an Array`
# **`id`**              | `integer`          | `not null, primary key`
# **`img_url`**         | `string`           | `not null`
# **`latitude`**        | `float`            | `not null`
# **`location_name`**   | `string`           | `not null`
# **`longitude`**       | `float`            | `not null`
# **`name`**            | `string`           | `not null`
# **`net_options`**     | `integer`          | `default([]), is an Array`
# **`net_url`**         | `string`           |
# **`nickname`**        | `boolean`          | `default(FALSE)`
# **`other_net_ids`**   | `integer`          | `default([]), is an Array`
# **`privacy`**         | `boolean`          | `not null`
# **`tags_id`**         | `integer`          |
# **`updated_at`**      | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_button_nets_on_tags_id`:
#     * **`tags_id`**
#

# => NET_OPTIONS ARRAY OF BOOLEAN MEANING  [0-PRICE_MODULE, 1-CAUSE_MODULE, 2-TRANSPORT_MODULE, 3-NO_MAP_MODULE]
#

class ButtonNet < ApplicationRecord

  include Geokit::Geocoders

  acts_as_mappable :lat_column_name => :latitude, :lng_column_name => :longitude

  belongs_to :creator, class_name: 'User'

  has_many :buttons

  # has_many :tags
  has_many :button_net_tags
  has_many :tags, through: :button_net_tags, source: :tag

  # has_many :tags, through: :net_tags, source: :tag


  has_many :users, through: :admin_users, source: :user
  # has_many :users, through: :blocked_users, source: :user

  validates :name, presence: true, uniqueness: true

  # after_save :set_lat_and_long


  # def self.find_with_token name, token
  #   self.find_by name: name, authentication_tokens: { body: token }
  # end

  after_commit :update_net_counter

  def update_net_counter
  end

  def inactive_button_nets
    ButtonNet.where.not(active: true).count
  end

  # def set_lat_and_long
  #   if (self.latitude.blank? or self.longitude.blank?) and self.full_address.present?
  #     loc = GoogleGeocoder.geocode(self.full_address)
  #     self.latitude = loc.lat
  #     self.longitude = loc.lng
  #     if self.latitude.present? && self.longitude.present? && self.latitude.is_a?(Numeric) && self.longitude.is_a?(Numeric)
  #       self.save
  #     end
  #   end
  # end


end
