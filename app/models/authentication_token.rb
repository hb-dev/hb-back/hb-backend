# ## Schema Information
#
# Table name: `authentication_tokens`
#
# ### Columns
#
# Name                | Type               | Attributes
# ------------------- | ------------------ | ---------------------------
# **`body`**          | `string`           |
# **`created_at`**    | `datetime`         | `not null`
# **`id`**            | `integer`          | `not null, primary key`
# **`ip_address`**    | `string`           |
# **`last_used_at`**  | `datetime`         |
# **`updated_at`**    | `datetime`         | `not null`
# **`user_agent`**    | `string`           |
# **`user_id`**       | `integer`          |
#
# ### Indexes
#
# * `index_authentication_tokens_on_user_id`:
#     * **`user_id`**
#
# ### Foreign Keys
#
# * `fk_rails_...`:
#     * **`user_id => users.id`**
#

class AuthenticationToken < ApplicationRecord
  belongs_to :user
end
