# ## Schema Information
#
# Table name: `button_net_tags`
#
# ### Columns
#
# Name                 | Type               | Attributes
# -------------------- | ------------------ | ---------------------------
# **`button_net_id`**  | `integer`          |
# **`created_at`**     | `datetime`         | `not null`
# **`id`**             | `integer`          | `not null, primary key`
# **`tag_id`**         | `integer`          |
# **`updated_at`**     | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_button_net_tags_on_button_net_id`:
#     * **`button_net_id`**
# * `index_button_net_tags_on_tag_id`:
#     * **`tag_id`**
#

class ButtonNetTag < ApplicationRecord
  belongs_to :button_net
  belongs_to :tag
end
