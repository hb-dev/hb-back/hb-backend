#     <Helpbuttons, collaborative network-making software>
#     Copyright (C) 2013-2121  Helpbuttons
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
class UserTags::Query < Application

  attr_reader :params

  def initialize params
    @params = params
  end

  def exec
    query_tag
  end

  protected
  def query_tag
    tags = UserTag.all.order(active_buttons_counter: :desc)
    if tag_params[:filter]

      tags = tags.where("search_name ILIKE ?", "%#{tag_params[:name].downcase}%").order(active_buttons_counter: :desc) unless tag_params[:name].blank?

      tags = tags.uniq{|t| t.search_name}
      if tag_params[:popular].nil? || tag_params[:popular]!="true"
        tags = tags.order(active_buttons_counter: :desc).first(50)
      else
        tags = tags.order(active_buttons_counter: :desc).first(20)
      end
    else
      tags = UserTag.none
    end
    return tags
  end

  def tag_params
    {
      filter: !params.try(:[], :filter).blank?,
      name: params.try(:[], :filter).try(:[], :name),
      popular: params.try(:[], :filter).try(:[], :popular)
    }

  end

end
