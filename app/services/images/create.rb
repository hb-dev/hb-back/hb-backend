class Images::Create < Application

  attr_reader :params

  def initialize params
    @params = params
  end

  def exec
    build_image
  end

  protected
  def build_image
    Image.new image_params
  end

  def image_params
    {
      imageable_id: params['imageable-id'],
      imageable_type: params['imageable-type'].try(:capitalize).try(:singularize),
      image_property: params['imageable-property'],
      file: params[:file]
    }
  end
end
