class Buttons::Search < Application

  attr_reader :params, :button, :current_user

  def initialize params, current_user
    @params = params
    if !current_user.nil?
      @current_user = current_user
    end
  end

  def exec
    buttons,meta_info = search_button
    if !params[:buttonNet].nil?
      @buttonNetIds = []
      if params[:otherNets].nil?
        @buttonNet = ButtonNet.find_by(name: params[:buttonNet])
        if !@buttonNet.nil?
          @buttonNet.other_net_ids.each do |id|
            @buttonNetIds.push(id)

          end
          @buttonNetIds.push(@buttonNet.id)
        end
      else
        @buttonNet = ButtonNet.where(name: params[:otherNets].split(',').push(params[:buttonNet]))
        if !@buttonNet.nil?
          @buttonNet.each do |net|
            @buttonNetIds.push(net.id)
            net.other_net_ids.each do |id|
              @buttonNetIds.push(id)
            end
          end
        end
      end
      if !@buttonNet.nil?
        if !buttons.nil?
          unless buttons.kind_of?(Array)
            buttonsArr = Array(buttons);
            if buttonsArr.length > 0
              buttons = buttons.where('button_net_id && ARRAY[?]', @buttonNetIds)
              unless buttons.kind_of?(Array)
                buttonsArr = Array(buttons);
              end
            end
          else
            buttonsArr = buttons;
            if buttonsArr.length > 0
              buttons = buttons.where('button_net_id && ARRAY[?]', @buttonNetIds)
              unless buttons.kind_of?(Array)
                buttonsArr = Array(buttons);
              end
            end
          end
          if buttonsArr.length > 0
            if buttonsArr.length > 1
              if !@current_user.nil? && @current_user.admin
                buttons = buttons.shown_admin?(true, @current_user) unless buttons.blank?
              elsif !@current_user.nil? && @current_user.super_admin
                buttons = buttons.shown_super_admin?(true) unless buttons.blank?
              else
                buttons = buttons.shown?(true) unless buttons.blank?
              end
            else
              if !@current_user.nil? && @current_user.admin
                if buttons.first.nil?
                  if (buttons.active == false || buttons.deactivated_user == true) || (buttons.first.active == false || buttons.first.creator_id != @current_user.id)
                    buttons = nil
                  end
                else
                  if (buttons.first.active == false || buttons.first.deactivated_user == true) || (buttons.first.active == false || buttons.first.creator_id != @current_user.id)
                    buttons = nil
                  end
                end
              elsif !@current_user.nil? && @current_user.super_admin
                if buttons.first.deactivated_user == true
                  buttons = nil
                end
              else
                if buttons.first.active == false || buttons.first.deactivated_user == true
                  buttons = nil
                end
              end
            end
          end
        end
        if buttons.kind_of?(Object) && Array(buttons).length === 1
          return Array(buttons),meta_info
        end
        return buttons,meta_info
      end
      return buttons,meta_info
    else
      if Array(buttons).length == 1
        if buttons.class.to_s == 'Button'
          buttons = Button.all.where(id: buttons.id)
        end
      end
      if !@current_user.nil? && @current_user.admin
        buttons = buttons.shown_admin?(true, @current_user) unless buttons.blank?
        buttonsNetIds = []
        buttons.each do |button|
          countNetId = 0
          if !button.button_net_id.nil? && !button.button_net_id[countNetId].nil?
            if ButtonNet.all.where(id: button.button_net_id[countNetId]).blank?
              button.update(button_net_id: []);
            else
              button.button_net_id.each do |id|
                if !ButtonNet.find(button.button_net_id[countNetId]).privacy
                  buttonsNetIds.push(button.button_net_id[countNetId])
                  break
                else
                  countNetId = countNetId + 1
                end
              end
            end
          end
        end
        if buttonsNetIds.length > 0
          buttons = buttons.where('button_net_id = ? OR (button_net_id && ARRAY[?])', '{}', buttonsNetIds)
        else
          if buttons.length > 0
            buttons = buttons.where('button_net_id = ?', '{}')
          else
            buttons = Button.where('id = 99999999999999')
          end
        end
      elsif !@current_user.nil? && @current_user.super_admin
        buttons = buttons.shown_super_admin?(true) unless buttons.blank?
      else
        buttons = buttons.shown?(true) unless buttons.blank?
        buttonsNetIds = []
        buttons.each do |button|
          countNetId = 0
          if !button.button_net_id.nil? && !button.button_net_id[countNetId].nil?
            if ButtonNet.all.where(id: button.button_net_id[countNetId]).blank?
              button.update(button_net_id: []);
            else
              button.button_net_id.each do |id|
                if !ButtonNet.find(button.button_net_id[countNetId]).privacy
                  buttonsNetIds.push(button.button_net_id[countNetId])
                  break
                else
                  countNetId = countNetId + 1
                end
              end
            end
          end
        end
        if buttonsNetIds.length > 0
          buttons = buttons.where('button_net_id = ? OR (button_net_id && ARRAY[?])', '{}', buttonsNetIds)
        else
          if buttons.length > 0
            buttons = buttons.where('button_net_id = ?', '{}')
          else
            buttons = Button.where('id = 99999999999999')
          end
        end
      end
      return buttons,meta_info
    end
  end

  def params
    @params.to_unsafe_hash
  end

  protected
  def search_button
    search_type = ::Search::GeoCodeSearch.search_type(search_params)
    searcher = ::Search::GeoCodeSearch.get_searcher(search_type,search_params)
    searcher.search_buttons
  end

  def search_params
    ActionController::Parameters.new(params).permit(allowed_params)
  end

  def allowed_params
    %i[ adr adr2 lat lng swl swlng nel nelng id searchTags buttonNet otherNets size page multi]
  end

end
