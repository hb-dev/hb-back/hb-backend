class Buttons::Delete < Application

  attr_reader :button, :current_user

  def initialize button, current_user
    @button = button
    @current_user = current_user
  end

  def exec
    delete_button
  end

  protected
  def delete_button
    @button.chats.each do |chat|
      UserNotifierMailer.deleted_chat(chat.user, chat, true).deliver
    end
    return @button.destroy 
  end

end
