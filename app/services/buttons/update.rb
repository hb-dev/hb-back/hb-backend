class Buttons::Update < Application

  attr_reader :params, :button

  def initialize params, button
    @params = params
    @button = button
  end

  def exec
    if event_params[:event]
      update_with_event
    else
      update_button
    end
  end

  def params
    @params.to_unsafe_hash
  end

  protected

  def update_button
    @button.assign_attributes button_params
    return :unathorized if @button.active == true && ((@button.button_type=="offer" && @button.offer_tags.blank?) || (@button.button_type=="need" && @button.needed_tags.blank?) || (@button.button_type=="change" && (@button.offer_tags.blank? || @button.needed_tags.blank?)))
    @button
  end

  def update_with_event
    return :unathorized if @button.active == true && ((@button.button_type=="offer" && @button.offer_tags.blank?) || (@button.button_type=="need" && @button.needed_tags.blank?) || (@button.button_type=="change" && (@button.offer_tags.blank? || @button.needed_tags.blank?)))
    @button.assign_attributes(shared_counter: (@button.shared_counter + 1)) if event_params[:command] == "button-shared"
    if event_params[:command] == "button-transfered"
      @transf = TransferedButton.where(user_id: @button.creator_id, button_id: @button.id)
      if @transf.nil?
        @transf.first.destroy
      end
      @button.update(creator_id: button_params[:creator_id])
    end
    @button
  end

  def button_params
    ActiveModelSerializers::Deserialization.jsonapi_parse params, only: allowed_params
  end

  def allowed_params
    %i[ description button-type full-address latitude longitude to-latitude to-longitude offer-tags needed-tags swap active location-name to-location-name date creator periodic-date button-net-id multi]
  end

  def event_params
    action_event = ActiveModelSerializers::Deserialization.jsonapi_parse params, only: allowed_event_params
    return {
      event: !action_event.try(:[], :action_event).blank?,
      command: action_event.try(:[], :action_event)
    }
  end

  def allowed_event_params
    %i[ action-event ]
  end

end
