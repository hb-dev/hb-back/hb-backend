class Tags::Create < Application

  attr_reader :params

  def initialize params
    @params = params
  end

  def exec
    build_basic_tag
  end

  protected
  def build_basic_tag
    tag = Tag.new tag_params
  end

  def tag_params
    ActiveModelSerializers::Deserialization.jsonapi_parse params, only: allowed_params
  end

  def allowed_params
    %i[name]
  end
end
