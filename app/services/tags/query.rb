class Tags::Query < Application

  attr_reader :params

  def initialize params
    @params = params
  end

  def exec
    query_tag
  end

  protected
  def query_tag
    tags = Tag.all.order(active_buttons_counter: :desc)
    if tag_params[:filter]

      tags = tags.where("search_name ILIKE ?", "%#{tag_params[:name].downcase}%").order(active_buttons_counter: :desc) unless tag_params[:name].blank?

      tags = tags.uniq{|t| t.search_name}
      if tag_params[:popular].nil? || tag_params[:popular]!="true"
        tags = tags.order(active_buttons_counter: :desc).first(50)
      else
        tags = tags.order(active_buttons_counter: :desc).first(20)
      end
    else
      tags = Tag.none
    end
    return tags
  end

  def tag_params
    {
      filter: !params.try(:[], :filter).blank?,
      name: params.try(:[], :filter).try(:[], :name),
      popular: params.try(:[], :filter).try(:[], :popular)
    }

  end

end
