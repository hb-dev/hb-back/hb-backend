class Chats::Create < Application

  attr_reader :params, :current_user

  def initialize params, current_user
    @params = params
    @current_user = current_user
  end

  def exec
    build_basic_chat
  end

  protected
  def build_basic_chat
    chat = Chat.new chat_params
    chat.user = current_user
    chat
  end

  def chat_params
    ActiveModelSerializers::Deserialization.jsonapi_parse params, only: allowed_params
  end

  def allowed_params
    %i[button]
  end
end
