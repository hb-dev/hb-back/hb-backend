#     <Helpbuttons, collaborative network-making software>
#     Copyright (C) 2013-2121  Helpbuttons
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
class Chats::Query < Application

  attr_reader :params, :current_user

  def initialize params, current_user
    @params = params
    @current_user = current_user
  end

  def exec
    query_chat
  end

  protected
  def query_chat
    Chat.find_by chat_params
  end

  def chat_params
    {
      button_id: params[:button],
      user: current_user
    }
  end
end
