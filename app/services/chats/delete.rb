class Chats::Delete < Application

  attr_reader :chat, :current_user

  def initialize chat, current_user
    @chat = chat
    @current_user = current_user
  end

  def exec
    delete_chat
  end

  protected
  def delete_chat
    if @chat.destroy
      UserNotifierMailer.deleted_chat(@chat.user, @chat, false).deliver
      return true
    else
      return false
    end
  end

end
