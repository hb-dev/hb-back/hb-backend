class Messages::Create < Application

  attr_reader :params, :current_user

  def initialize params, current_user
    @params = params
    @current_user = current_user
    @button = set_button
  end

  def exec
    build_basic_message
  end

  protected
  def build_basic_message
    return :unathorized if @button.deactivated_user?
    return :disabled unless @button.active?
    message = Message.new message_params
    message.user = current_user
    message
  end

  def message_params
    ActiveModelSerializers::Deserialization.jsonapi_parse params, only: allowed_params
  end

  def set_button
    Chat.find((ActiveModelSerializers::Deserialization.jsonapi_parse params, only: :chat)[:chat_id]).try(:button)
  end

  def allowed_params
    %i[body chat created_at]
  end
end
