class Messages::Query < Application

  attr_reader :params, :current_user

  def initialize params
    @params = params
  end

  def exec
    query_message
  end

  protected
  def query_message
    Message.where message_params
  end

  def message_params
    { id: params[:id] }
  end
end
