class Users::UpdateOwnedButtons < Application

  attr_reader :user


  def initialize user
    @user = user
  end

  def exec
    update_owned_buttons
  end

  protected
  def update_owned_buttons
    @user.owned_buttons.update_all(deactivated_user: !@user.active)
    @user.owned_buttons.each do |button|
      button.tags.each do |tag|
        tag.update_counter
        # tag.save
      end
    end
    @user
  end

end
# tag = Tag.new
# user = Users.new
#
# d.z(a)
