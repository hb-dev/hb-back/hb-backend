class Users::Update < Application

  attr_reader :params, :user

  def initialize params, user
    @params = params
    @user   = user
  end

  def exec
      update_user
  end

  def params
    @params.to_unsafe_hash
  end

  protected
  def update_user
    user.assign_attributes user_params
    user
  end

  def user_params
    ActiveModelSerializers::Deserialization.jsonapi_parse params, only: allowed_params
  end

  def allowed_params
    %i[nickname name phone description location app-language avatar active show-phone use-external-conv user-telegram use-telegram use-whatsapp deliver-interests tags-distance tags-longitude tags-latitude likes liked blocked blocked_by]
  end

end
