class Languages::Retrieve < Application

  attr_reader :params

  def initialize params
    @params = params
  end

  def exec
    retrieve_languages
  end

  protected
  def retrieve_languages
    languages = []
    unless params[:data].nil?
      params[:data].each do |language|
        render_errors(I18n.t("activerecord.errors.user.languages.wrong_type"), I18n.t("activerecord.errors.user.languages.wrong_type_description"), 403) and return unless %w{languages}.include? language[:type]
        new_language = Language.find(language[:id])
        languages << new_language
      end
    end
    return languages
  end

end
