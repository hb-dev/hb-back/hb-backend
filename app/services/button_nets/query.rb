#     <Helpbuttons, collaborative network-making software>
#     Copyright (C) 2013-2121  Helpbuttons
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
class ButtonNets::Query < Application

  attr_reader :params

  def initialize params, current_user
    @params = params
    @current_user = current_user
  end

  def exec
    query_button_net
  end

  protected
  def query_button_net
    button_nets = ButtonNet.all
    if button_net_params[:filter]
      button_nets = button_nets.where("active=true AND privacy=false AND name ILIKE ?", "%#{button_net_params[:name].downcase}%") unless button_net_params[:name].blank?
      button_nets = button_nets.uniq{|t| t.name}
      if button_net_params[:popular].nil? || button_net_params[:popular]!="true"
        button_nets = button_nets.first(50)
      else
        button_nets = button_nets.first(20)
      end
    else
      if button_nets.nil?
        button_nets = ButtonNet.none
      elsif !params[:creator_id].nil?
          button_nets = button_nets.where("active=true AND creator_id=?", [@current_user.id]) unless params[:creator_id].blank?
      elsif !params[:id].nil?
          button_nets = button_nets.where("active=true AND id=?", params[:id]) unless params[:id].blank?
      else
        if @current_user.nil?
          button_nets = button_nets.where("active=true AND privacy = false")
        else
          button_nets1 = button_nets.where("active=true AND privacy = false")
          button_nets2 = button_nets.where("allowed_users && ARRAY[?]", [@current_user.id])
          button_nets2 = button_nets2.where("active=true AND privacy = true")
          button_nets = button_nets2 + button_nets1
        end
        button_nets = button_nets.first(20)
      end
    end
    return button_nets
  end

  def button_net_params
    if params.try(:[], :filter)
      {
        filter: !params.try(:[], :filter).blank?,
        id: !params.try(:[], :id).blank?,
        name: params.try(:[], :name),
        popular: params.try(:[], :popular),
        creator_id: !params.try(:[], :creator_id).blank?
      }
    else
      {
        filter: !params.try(:[], :filter).blank?,
        id: !params.try(:[], :id).blank?,
        name: params.try(:[], :filter).try(:[], :name),
        popular: params.try(:[], :filter).try(:[], :popular),
        creator_id: !params.try(:[], :creator_id).blank?
      }
    end
  end

end
