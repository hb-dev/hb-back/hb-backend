#     <Helpbuttons, collaborative network-making software>
#     Copyright (C) 2013-2121  Helpbuttons
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
class ButtonNets::Create < Application

  attr_reader :params, :current_user

  def initialize params, current_user
    @params = params
    @current_user = current_user
  end

  def exec
    build_basic_button_net
  end

  protected
  def build_basic_button_net
    button_net = ButtonNet.new button_net_params
    button_net.creator = current_user
    # button_net.location_name = "Test"
    button_net.save
    button_net
  end

  def button_net_params
    ActiveModelSerializers::Deserialization.jsonapi_parse params, only: allowed_params
  end

  def allowed_params
    %i[ nickname name description active img-url latitude location-name longitude net-url privacy updated-at net-options creator-id]
  end
end
