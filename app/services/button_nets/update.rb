class ButtonNets::Update < Application

  attr_reader :params, :button_net

  def initialize params, button_net, current_user
    @params = params
    @button_net = button_net
    @current_user = current_user
  end

  def exec
      update_button_net
  end

  def params
    @params.to_unsafe_hash
  end

  protected
  def update_button_net
    button_net.assign_attributes button_net_params
    netTagIds = params[:data][:relationships][:"tags"][:data]
    button_net.tags = []
    netTagIds.each do |tagId|
      if Tag.exists?(tagId[:id])
        button_net.tags.push(Tag.find(tagId[:id]))
      end
    end
    button_net
  end

  def button_net_params
    if @button_net.admin_users.include?(@current_user.id)
      ActiveModelSerializers::Deserialization.jsonapi_parse params, only: allowed_params_admin
    else
      ActiveModelSerializers::Deserialization.jsonapi_parse params, only: allowed_params
    end
  end

  def allowed_params
    %i[ nickname name description active img-url latitude location-name longitude net-url privacy updated-at net-options blocked-users other-net-ids admin-users allowed-users creator-id tags]
  end

  def allowed_params_admin
    %i[ nickname name description active img-url latitude location-name longitude blocked-users]
  end

  def allowed_event_params
    %i[ action-event ]
  end

end
