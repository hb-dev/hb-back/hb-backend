include ActionView::Helpers::NumberHelper

class Image::ShowSerializer < ActiveModel::Serializer
  attributes :id

  attribute :name do
    object.file_file_name
  end

  attribute :size do
    object.file_file_size.blank? ? "- -" : number_to_human_size(object.file_file_size)
  end

  attribute :url do
    object.file.url
  end

  attribute :medium do
    object.file.url("medium")
  end

  attribute :thumb do
    object.file.url("thumb")
  end

  attribute :small_thumb do
    object.file.url("small")
  end

  attribute :tiny_thumb do
    object.file.url("tiny")
  end

  attributes :created_at

end
