class Message::ShowSerializer < ActiveModel::Serializer
  attributes :id, :body, :created_at

  has_one :user, serializer: User::ChatShowSerializer
  has_one :chat, serializer: Chat::ShowSerializer
end
