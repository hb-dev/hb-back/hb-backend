class Chat::ShowSerializer < ActiveModel::Serializer
  attributes :id
  attributes :multi
  attribute :'unread-messages' do
    object.unread_messages(instance_options[:current_user])
  end

  attribute :'last-message-date' do
    object.messages.last.try(:created_at)
  end

  has_one :button_creator, serializer: User::ChatShowSerializer
  has_one :user, serializer: User::ChatShowSerializer
  has_one :button, serializer: Button::ChatShowSerializer

  has_many :messages do
    include_data false
    # link :self, "/api/v1/chats/#{object.id}/relationships/messages"
    link :related,  "/api/v1/chats/#{object.id}/messages"
  end

end
