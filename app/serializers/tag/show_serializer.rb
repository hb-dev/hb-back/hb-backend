class Tag::ShowSerializer < ActiveModel::Serializer
  attributes :name, :active_buttons_counter
end
