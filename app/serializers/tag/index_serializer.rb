class Tag::IndexSerializer < ActiveModel::Serializer
  attributes :name, :active_buttons_counter
end
