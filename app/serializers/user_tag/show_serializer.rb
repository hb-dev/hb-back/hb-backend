class UserTag::ShowSerializer < ActiveModel::Serializer
  attributes :tag_type, :button_nets
  belongs_to :user, serializer: User::ShowSerializer
  belongs_to :tag, serializer: Tag::ShowSerializer
end
