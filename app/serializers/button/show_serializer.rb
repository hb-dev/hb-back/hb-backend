class Button::ShowSerializer < ActiveModel::Serializer
  # Common attributes
  attributes :description, :button_type, :active, :created_at, :swap, :date, :shared_counter, :periodic_date, :button_net_id, :multi

  # Location attributes
  attributes :full_address, :location_name, :to_location_name, :latitude, :longitude, :to_latitude, :to_longitude

  attribute :chats_counter do
    object.chats.count
  end

  has_one :creator, serializer: User::ShowSerializer

  # has_many :button_net_id

  has_many :chats do
    include_data false
    # link :self, "/api/v1/buttons/#{object.id}/relationships/chats"
    link :related,  "/api/v1/buttons/#{object.id}/chats"
  end

  has_many :offer_tags, serializer: Tag::ShowSerializer

  has_many :needed_tags, serializer: Tag::ShowSerializer

  has_one :image

  attribute :notifications_number do
    notifNumber = 0
    if !current_user.nil?
      if current_user.id == object.creator.id
        object.chats.each do |chat|
          notifNumber = notifNumber + chat.unread_messages(object)
        end
      end
    end
    notifNumber
  end

end
