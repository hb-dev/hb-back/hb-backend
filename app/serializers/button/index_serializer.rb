class Button::IndexSerializer < ActiveModel::Serializer
  # Common attributes
  attributes :description, :button_type, :active, :created_at, :swap, :date, :shared_counter, :periodic_date, :button_net_id, :multi

  # Location attributes
  attributes :full_address, :location_name, :to_location_name, :latitude, :longitude, :to_latitude, :to_longitude

  attribute :chats_counter do
    object.chats.count
  end

  attribute :chat_unread do
    if !current_user.nil?
      @chat = object.chats.find_by(user_id: current_user.id)
      if !@chat.nil?
        @count = @chat.unread_messages(current_user) > 0
      end
    end
    @count
  end

  attribute :is_editable do
    @editable = false
    if !current_user.nil?
      object.button_net_id.each do |id|
        btnNet = ButtonNet.where(id: id)
        if btnNet.length > 0 && (ButtonNet.find(id).admin_users.include?(current_user.id) || ButtonNet.find(id).creator == current_user)
          @editable = true
        end
      end
    end
    @editable
  end

  attribute :is_editable_id do
    @editable = false
    if !current_user.nil?
      object.button_net_id.each do |id|
        btnNet = ButtonNet.where(id: id)
        if btnNet.length > 0 && (ButtonNet.find(id).admin_users.include?(current_user.id) || ButtonNet.find(id).creator == current_user)
          @editable = id
        end
      end
    end
    @editable
  end

  has_one :creator, serializer: User::ShowSerializer

  has_many :chats do
    include_data false
    # link :self, "/api/v1/buttons/#{object.id}/relationships/chats"
    link :related,  "/api/v1/buttons/#{object.id}/chats"
 end

  has_many :offer_tags, serializer: Tag::ShowSerializer

  has_many :needed_tags, serializer: Tag::ShowSerializer

  has_one :image

  attribute :notifications_number do
    notifNumber = 0
    if !current_user.nil?
      if current_user.id == object.creator.id
        object.chats.each do |chat|
          notifNumber = notifNumber + chat.unread_messages(object)
        end
      end
    end
    notifNumber
  end

end
