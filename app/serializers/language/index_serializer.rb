class Language::IndexSerializer < ActiveModel::Serializer
  attributes :id#, :code

  attribute :name do
    object.translated_name
  end

  # has_many :users
end
