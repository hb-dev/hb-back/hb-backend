class Language::ShowSerializer < ActiveModel::Serializer
  attributes :id

  attribute :name do
    object.translated_name
  end
  
end
