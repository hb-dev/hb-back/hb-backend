class User::LoginSerializer < ActiveModel::Serializer
  attributes :id, :email, :nickname, :name, :phone, :description, :app_language, :location,
  :active, :show_phone, :use_telegram, :use_whatsapp, :user_telegram, :use_external_conv,
  :admin, :super_admin, :deliver_interests, :tags_longitude, :tags_latitude, :tags_distance, :get_button_with_msg,
  :liked, :likes, :blocked, :blocked_by, :get_pressed_btn_counter

  attribute :internal_id do
    object.id
  end

  # To make sure the password is not kept in memory in Ember
  attribute :current_password do nil end
  attribute :password do nil end
  attribute :password_confirmation do nil end

  attribute :owned_buttons_counter do
    object.owned_buttons.count
  end

  has_one :avatar
  has_many :user_tags, serializer: UserTag::InterestSerializer
  has_many :languages, serializer: Language::IndexSerializer
  has_many :buttons
  has_many :owned_buttons
  has_many :button_nets, serializer: ButtonNets::IdSerializer
 #  has_many :button_nets do
 #    include_data true
 #    # link :self, "/api/v1/buttons/#{object.id}/relationships/chats"
 #    # link :related,  "/api/v1/button-nets/#{object.id}"
 # end

  def id
    "current"
  end

  attribute :notifications_number do
    notifNumber = 0
    object.owned_buttons.each do |button|
      button.chats.each do |chat|
        notifNumber = notifNumber + chat.unread_messages(object)
      end
    end
    notifNumber
  end


end
