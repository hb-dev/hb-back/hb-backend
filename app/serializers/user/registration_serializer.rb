class User::RegistrationSerializer < ActiveModel::Serializer
  attributes :id, :email, :random_password, :active

  has_one :avatar, serializer: Image::ShowSerializer
end
